#include "Kuid.h"

Kuid::Kuid(std::istream &istream) : Kuid() {
    load(istream);
}

void Kuid::load(std::istream& istream) {
    istream.read(reinterpret_cast<char*>(&_content_id), 4);
    istream.read(reinterpret_cast<char*>(&_author_id), 4);
    if (_author_id & 0x80000000) {
        _version = 0;
    } else {
        _version = (_author_id & 0xfe000000) >> 25;
        _author_id &= 0xffffff;
    }
}

void Kuid::save(std::ostream &ostream) const {
    ostream.write(reinterpret_cast<const char*>(&_content_id), 4);
    auto tmp = _author_id;
    if (tmp >= 0) {
        tmp |= _version << 25;
    }
    ostream.write(reinterpret_cast<const char*>(&tmp), 4);
}

std::ostream& operator<< (std::ostream& ostream, const Kuid& kuid) {
    if (kuid._version) {
        ostream << "<kuid2:" << kuid._author_id << ':' << kuid._content_id << ':' << kuid._version << '>';
    } else {
        ostream << "<kuid:" << kuid._author_id << ':' << kuid._content_id << '>';
    }
    return ostream;
}
