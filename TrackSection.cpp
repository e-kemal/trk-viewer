#include "TrackSection.h"
#include "vectorsHelper.h"
#include <iomanip>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/VertexArray.hpp>

void TrackSection::loadBody(std::istream &istream) {
    int version;
    istream.read(reinterpret_cast<char*>(&version), 4);
std::cout << "\t\t\tversion (12): " << version << std::endl;
    istream.read(reinterpret_cast<char*>(&vertex1_id), 4);
    istream.read(reinterpret_cast<char*>(&vertex2_id), 4);
std::cout << "\t\t\tvertex1: " << vertex1_id << ", vertex2: " << vertex2_id << std::endl;
    kuid1.load(istream);
std::cout << "\t\t\tkuid: " << kuid1 << std::endl;
    istream.read(reinterpret_cast<char*>(&flags), 4);
std::cout << "\t\t\tflags: " << std::hex << std::setfill('0') << std::setw(8) << flags << std::dec;
    auto flags2 = flags;
    if (flags & 0x20) {
        flags2 &= ~0x20;
std::cout << " pram";
    }
    if (flags & 0x100) {
        flags2 &= ~0x100;
std::cout << " v1_height";
    }
    if (flags & 0x10) {
        flags2 &= ~0x10;
std::cout << " v2_height";
    }
    if (flags & 0x10000) {
        flags2 &= ~0x10000;
std::cout << " is_track";
    }
    if (flags & 0x20000) {
        flags2 &= ~0x20000;
std::cout << " is_road";
    }
    if (flags & 0x8) {
        flags2 &= ~0x8;
std::cout << " is_bridge1";
    }
    if (flags & 0x400) {
        flags2 &= ~0x400;
std::cout << " is_bridge2";
    }
    if (flags & 0x408 && getType() != "tkBr") {
std::cerr << "track: " << _id << ", has 0x408, but not is bridge" << std::endl;
    }
    if ((flags & 0x408) != 0x408 && getType() != "tkSt") {
std::cerr << "track: " << _id << ", not have 0x408, but is bridge" << std::endl;
    }
    if (flags & 0x4) {
        flags2 &= ~0x4;
std::cout << " in_scenery1";
    }
    if (flags & 0x80) {
        flags2 &= ~0x80;
std::cout << " in_scenery2";
    }
    if (flags & 0x200) {
        flags2 &= ~0x200;
std::cout << " in_junction_scenery";
    }
    if (flags & 0x40) {
        flags2 &= ~0x40;
std::cout << " in_scenery_cycle";
    }
    if (version > 6) {
        flags2 ^= 0x0008d000;
    }
std::cout << ' ' << std::hex << std::setfill('0') << std::setw(8) << flags2 << std::dec << std::endl;
    if (flags2) {
std::cerr << "track: " << _id << ", flags: " << std::hex << std::setw(8) << std::setfill('0') << flags2 << std::dec << std::endl;
    }
    if (version < 5) {
        return;
    }
    istream.read(reinterpret_cast<char*>(&layer_id), 1);
std::cout << "\t\t\tlayer: " << static_cast<int>(layer_id) << std::endl;
    uint32_t len;
    istream.read(reinterpret_cast<char*>(&len), 4);
    if (len) {
        name.resize(len);
        istream.read(name.data(), len);
        if ('\0' == name.back()) {
            name.erase(name.length() - 1);
        }
    }
std::cout << "\t\t\tname: \"" << name << '\"' << std::endl;
    if (version < 7) {
        return;
    }
    istream.read(reinterpret_cast<char*>(&quality), 4);
std::cout << "\t\t\tquality: " << quality << std::endl;
    if (version < 8) {
        return;
    }
    istream.read(reinterpret_cast<char*>(&length), 4);
    istream.read(reinterpret_cast<char*>(&length2), 4);
std::cout << "\t\t\tlength(?): " << length << ", length2(?): " << length2 << std::endl;
if (length != length2) {
std::cerr << "id: " << _id << " - diff len" << std::endl;
}
    istream.read(reinterpret_cast<char*>(&tangent1.x), 4);
    istream.read(reinterpret_cast<char*>(&tangent1.y), 4);
    istream.read(reinterpret_cast<char*>(&tangent1.z), 4);
    istream.read(reinterpret_cast<char*>(&tangent2.x), 4);
    istream.read(reinterpret_cast<char*>(&tangent2.y), 4);
    istream.read(reinterpret_cast<char*>(&tangent2.z), 4);
std::cout << "\t\t\ttangent1: " << tangent1 << ", tangent2: " << tangent2 << std::endl;
    istream.read(reinterpret_cast<char*>(&data1.x), 4);
    istream.read(reinterpret_cast<char*>(&data2.x), 4);
    istream.read(reinterpret_cast<char*>(&delta.x), 4);
    istream.read(reinterpret_cast<char*>(&vertex1.x), 4);
    istream.read(reinterpret_cast<char*>(&data1.y), 4);
    istream.read(reinterpret_cast<char*>(&data2.y), 4);
    istream.read(reinterpret_cast<char*>(&delta.y), 4);
    istream.read(reinterpret_cast<char*>(&vertex1.y), 4);
    istream.read(reinterpret_cast<char*>(&data1.z), 4);
    istream.read(reinterpret_cast<char*>(&data2.z), 4);
    istream.read(reinterpret_cast<char*>(&delta.z), 4);
    istream.read(reinterpret_cast<char*>(&vertex1.z), 4);
std::cout << "\t\t\tdata1: " << data1 << ' ' << std::endl;
std::cout << "\t\t\tdata2: " << data2 << ' ' << std::endl;
std::cout << "\t\t\tvertex1: " << vertex1 << std::endl;
std::cout << "\t\t\tdelta: " << delta << ' ' << std::endl;

    uint32_t a1, a2;
    istream.read(reinterpret_cast<char*>(&a1), 4);
    istream.read(reinterpret_cast<char*>(&a2), 4);
std::cout << "\t\t\t??: " << std::hex << a1 << ' ' << a2 << std::dec << std::endl;
if (0x7f7fffff != a1) {
    std::cerr << "id: " << _id << ", a1: " << a1 << std::endl;
}
if (0x101 != a2) {
    std::cerr << "id: " << _id << ", a2: " << a2 << std::endl;
}

    box.load(istream);
std::cout << "\t\t\tbox: " << box << std::endl;
    istream.read(reinterpret_cast<char*>(&inTile.x), 2);
    istream.read(reinterpret_cast<char*>(&inTile.y), 2);
std::cout << "\t\t\t??: in tile: " << inTile << std::endl;
    istream.read(reinterpret_cast<char*>(&scenery_id), 4);
    if (flags & 0xc4 && scenery_id < 0) {
std::cerr << "track: " << _id << ", have 0xc4, but not in scenery" << std::endl;
    }
    if (!(flags & 0x84) && scenery_id >= 0) {
std::cerr << "track: " << _id << ", have not 0x84, but in scenery" << std::endl;
    }
    istream.read(reinterpret_cast<char*>(&len), 4);
    in_scenery_name.resize(len);
    istream.read(in_scenery_name.data(), len);
    if ('\0' == in_scenery_name.back()) {
        in_scenery_name.erase(in_scenery_name.length() - 1);
    }
std::cout << "\t\t\tscenery: " << scenery_id << ", name: \"" << in_scenery_name << '\"' << std::endl;
    int32_t b;
    istream.read(reinterpret_cast<char*>(&b), 4);
std::cout << "\t\t\t?: " << b;
    istream.read(reinterpret_cast<char*>(&inBridge), 4);
std::cout << ", bridge: " << inBridge << std::endl;
}

void TrackSection::saveBody(std::ostream &ostream) const {

}

void TrackSection::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    sf::Vector3f v1 = vertex1;
    v1.x += static_cast<float>(720 * inTile.x);
    v1.y += static_cast<float>(720 * inTile.y);
//    auto v2 = v1 + delta + data1 + data2;
//    auto t1 = v1 + delta / 3.f;
//    auto t2 = v1 + (2.f * delta + data2) / 3.f;
    sf::VertexArray curve(sf::LineStrip, 11);
    curve[0] = sf::Vertex(to2d(v1), sf::Color::Blue);
    auto tmp3v1 = 3.f * v1;
    auto tmp2deltaPd2P3v1 = 2.f * delta + data2 + tmp3v1;
    auto tmp3v1Pdelta = tmp3v1 + delta;
    for (int i = 1; i < 10; ++i) {
        auto t = .1f * static_cast<float>(i);
        curve[i] = sf::Vertex(to2d(
                t * (t * (t * (delta + data1 + data2 + tmp3v1Pdelta - tmp2deltaPd2P3v1)
                + (tmp3v1 - 2.f * tmp3v1Pdelta + tmp2deltaPd2P3v1))
                + (tmp3v1Pdelta - tmp3v1))
                + v1), sf::Color::Blue);
//        curve[i] = sf::Vertex(to2d(t * t * t * (v2 + 3.f * (t1 - t2) - v1) + 3.f * t * t * (v1 - 2.f * t1 + t2) + 3.f * t * (t1 - v1) + v1), sf::Color::Blue);
    }
    curve[10] = sf::Vertex(to2d(v1 + delta + data1 + data2), sf::Color::Blue);
    target.draw(curve);
//    sf::Vertex curve[] = {
//            sf::Vertex(to2d(v1), sf::Color::Blue),
//            sf::Vertex(to2d(v2), sf::Color::Blue),
//    };
//    target.draw(curve, 2, sf::Lines);

//    sf::Vertex line2[] = {
//            sf::Vertex(to2d(v1), sf::Color::Red),
//            sf::Vertex(to2d(t1), sf::Color::Red),
//            sf::Vertex(to2d(t2), sf::Color::Red),
//            sf::Vertex(to2d(v2), sf::Color::Red),
//    };
//    target.draw(line2, 4, sf::LineStrip);

//    curve[0] = sf::Vertex(to2d(v1), sf::Color::Green);
//    curve[1] = sf::Vertex(to2d(v1 + tangent1), sf::Color::Green);
//    target.draw(curve, 2, sf::Lines);
//
//    curve[0] = sf::Vertex(to2d(v2), sf::Color::Green);
//    curve[1] = sf::Vertex(to2d(v2 + tangent2), sf::Color::Green);
//    target.draw(curve, 2, sf::Lines);

//    curve[0] = sf::Vertex(to2d(v1), sf::Color::Yellow);
//    curve[1] = sf::Vertex(to2d(v1 + delta / 3.f), sf::Color::Yellow);
//    target.draw(curve, 2, sf::Lines);
//
//    curve[0] = sf::Vertex(to2d(v2), sf::Color::Yellow);
//    curve[1] = sf::Vertex(to2d(v1 + (2.f * delta + data2) / 3.f), sf::Color::Yellow);
//    target.draw(curve, 2, sf::Lines);
}

void TrackSection::render(wxDC& dc, const CoordinateTransformer& transformer) const {
    dc.SetPen(*wxBLUE_PEN);

    auto p1 = transformer(inTile, vertex1);
    auto tmp3v1 = 3.f * vertex1;
    auto tmp2deltaPd2P3v1 = 2.f * delta + data2 + tmp3v1;
    auto tmp3v1Pdelta = tmp3v1 + delta;
    for (int i = 1; i < 10; ++i) {
        auto t = .1f * static_cast<float>(i);
        auto p2 = transformer(inTile,
                              t * (t * (t * (delta + data1 + data2 + tmp3v1Pdelta - tmp2deltaPd2P3v1)
                                        + (tmp3v1 - 2.f * tmp3v1Pdelta + tmp2deltaPd2P3v1))
                                   + (tmp3v1Pdelta - tmp3v1))
                              + vertex1);
        dc.DrawLine(p1, p2);
        p1 = p2;
    }
    dc.DrawLine(p1, transformer(inTile, vertex1 + delta + data1 + data2));

//    wxPoint points[] = {
//            transformer(inTile, vertex1),
//            transformer(inTile, vertex1 + delta / 3.f),
//            transformer(inTile, vertex1 + (2.f * delta + data2) / 3.f),
//            transformer(inTile, vertex1 + delta + data1 + data2),
//    };
//    dc.DrawSpline(4, points);

//    dc.SetPen(*wxRED_PEN);
//    dc.SetBrush(*wxRED_BRUSH);
//    dc.DrawCircle(transformer(inTile, vertex1 + delta / 3.f), 3);
//    dc.DrawCircle(transformer(inTile, vertex1 + (2.f * delta + data2) / 3.f), 3);
}
