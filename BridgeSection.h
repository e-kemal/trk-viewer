#ifndef TRK_VIEWER_BRIDGESECTION_H
#define TRK_VIEWER_BRIDGESECTION_H


#include "TrackSection.h"

class BridgeSection : public TrackSection {
public:
    BridgeSection(const int32_t& id, const Kuid& kuid) : TrackSection(id, kuid) {};
    static constexpr EntityType _type = "tkBr";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
    void saveBody(std::ostream &ostream) const override;
};


#endif //TRK_VIEWER_BRIDGESECTION_H
