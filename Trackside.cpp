#include "Trackside.h"

void Trackside::loadBody(std::istream &istream) {
    Scenery::loadBody(istream);

    int32_t version, b = 0, c = 0;
    char d = 0, e;
    istream.read(reinterpret_cast<char*>(&version), 4);
    if (version > 2) {
        istream.read(reinterpret_cast<char *>(&b), 4);
        istream.read(reinterpret_cast<char *>(&c), 4);
    }
    if (version > 3) {
        istream.read(&d, 1);
    }
    istream.read(&e, 1);
std::cout << "\t\t\tversion(4): " << version << ' ' << b << ' ' << c << ' ' << static_cast<int>(d) << ", use radius: " << static_cast<int>(e) << std::endl;
    if (e) {
        float f1, f2;
        istream.read(reinterpret_cast<char*>(&f1), 4);
        istream.read(reinterpret_cast<char*>(&f2), 4);
std::cout << "\t\t\t?\?: " << f1 << ", radius: " << f2 << std::endl;
    }
    int32_t g, h;
    istream.read(reinterpret_cast<char*>(&g), 4);
    istream.read(reinterpret_cast<char*>(&h), 4);
    float i;
    istream.read(reinterpret_cast<char*>(&i), 4);
    char j;
    istream.read(&j, 1);
std::cout << "\t\t\t?\?: " << g << ", track: " << h << ", distance: " << i << ", orientation: " << static_cast<int>(j) << std::endl;
std::cout << "\t\t\t??:" << std::hex;
    for (int k = 0; k < 3; ++k) {
        char t;
        istream.read(&t, 1);
std::cout << ' ' << static_cast<int>(t);
    }
    char state, stateEx;
    istream.read(&state, 1);
    istream.read(&stateEx, 1);
std::cout << std::dec << ", state: " << static_cast<int>(state) << ", stateEx: " << static_cast<int>(stateEx) << std::endl;
//    char l;
//    istream.read(&l, 1);
//    uint32_t k;
//    istream.read(reinterpret_cast<char*>(&k), 4);
//std::cout << "\t\t\t??: " << static_cast<int>(l) << std::hex << ' ' << k << std::dec << std::endl;
}

void Signal::loadBody(std::istream &istream) {
    Trackside::loadBody(istream);

    int32_t a;
    char b, c;
    istream.read(reinterpret_cast<char*>(&a), 4);
    istream.read(&b, 1);
    istream.read(&c, 1);
std::cout << "\t\t\t?\?(2): " << a << ' ' << static_cast<int>(b) << ", automatic: " << static_cast<int>(c) << std::endl;
    std::string d;
    d.resize(4);
    istream.read(reinterpret_cast<char*>(d.data()), 4);
    int32_t a1, a2;
    int32_t soupSize;
    istream.read(reinterpret_cast<char*>(&a1), 4);
    istream.read(reinterpret_cast<char*>(&a2), 4);
    istream.read(reinterpret_cast<char*>(&soupSize), 4);
std::cout << "\t\t\t??: " << d << ", " << a1 << ' ' << a2 << ", size: " << soupSize << std::endl;
    std::string soup;
    soup.resize(soupSize);
    istream.read(soup.data(), soupSize);
}
