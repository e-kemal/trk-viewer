#ifndef TRK_VIEWER_WORLDCOORDINATE_H
#define TRK_VIEWER_WORLDCOORDINATE_H

#include <fstream>
#include <SFML/System/Vector2.hpp>
#include <SFML/System/Vector3.hpp>


class WorldCoordinate {
public:
    sf::Vector2<int16_t> _baseboard;
    sf::Vector3f _offset;
    WorldCoordinate() = default;
    explicit WorldCoordinate(std::istream& istream);
    void load(std::istream& istream);
    void save(std::ostream& ostream) const;
};

std::ostream& operator<< (std::ostream& ostream, const WorldCoordinate& coordinate);

#endif //TRK_VIEWER_WORLDCOORDINATE_H
