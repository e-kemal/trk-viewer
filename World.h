#include <filesystem>
#include <map>
#include <SFML/Graphics/Drawable.hpp>
#include <wx/dc.h>
#include "Tile.h"

#ifndef TRK_VIEWER_WORLD_H
#define TRK_VIEWER_WORLD_H


class World : public sf::Drawable {
    struct tilesCompare {
        bool operator()(const auto& l, const auto& r) const {
            return std::tie(l.x, l.y) < std::tie(r.x, r.y);
        }
    };
    std::map<sf::Vector2<int16_t>, Tile, tilesCompare> tiles;
    std::map<int32_t, std::shared_ptr<Entity>> allVtx;
    std::map<int32_t, std::shared_ptr<Entity>> allTrk;
    std::map<int32_t, std::shared_ptr<Entity>> allRules;
    std::map<int32_t, std::shared_ptr<Entity>> allObjects;
    sf::Vector2<int16_t> viewAreaMin{std::numeric_limits<int16_t>::min(), std::numeric_limits<int16_t>::min()};
    sf::Vector2<int16_t> viewAreaMax{std::numeric_limits<int16_t>::max(), std::numeric_limits<int16_t>::max()};
    bool loadEntities(std::istream& tile, const std::optional<sf::Vector2<int16_t>>& tilePosition = std::nullopt);
    bool loadEntitiesLegacy(std::istream& tile);
    bool loadEntityProperties(std::istream& tile, int32_t version);
    bool loadFile(std::istream& tile, int32_t fileType, const std::optional<sf::Vector2<int16_t>>& tilePosition = std::nullopt);
    bool loadFiles(const std::filesystem::path& path, const std::string& filename, const std::string& ext);
    bool load(const std::filesystem::path& path, const std::string& filename);
    bool loadEnv(std::istream& in, bool waterColor = true);
public:
    World() = default;
    ~World() override;
    bool loadMap(const std::filesystem::path& path, const std::string& filename);
    bool loadProfile(const std::filesystem::path& path, const std::string& filename);
    bool save(const std::filesystem::path& path) const;
    void close();
    void setViewArea(const sf::Vector2<int16_t>& min, const sf::Vector2<int16_t>& max) {
        viewAreaMin = min;
        viewAreaMax = max;
    }
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
    void render(wxDC& dc, const CoordinateTransformer& transformer);
};


#endif //TRK_VIEWER_WORLD_H
