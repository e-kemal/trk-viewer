#ifndef TRK_VIEWER_SCENERY_H
#define TRK_VIEWER_SCENERY_H


#include "Entity.h"
#include "WorldCoordinate.h"

class SceneryAbstract : public Entity {
public:
    WorldCoordinate coordinate;
    float rotate{}, incline{};
    float f{}, height{};
    uint8_t layer{};
protected:
    SceneryAbstract(const int32_t& id, const Kuid& kuid) : Entity(id, kuid) {};
    void loadBody(std::istream &istream) override;
    void saveBody(std::ostream &ostream) const override;
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
public:
    void render(wxDC& dc, const CoordinateTransformer& transformer) const override;
};

class SceneryBase : public SceneryAbstract {
public:
    SceneryBase(const int32_t& id, const Kuid& kuid) : SceneryAbstract(id, kuid) {}
    static constexpr EntityType _type = "mobs";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
};

class ScenerySpeedTree : public SceneryBase {
public:
    ScenerySpeedTree(const int32_t& id, const Kuid& kuid) : SceneryBase(id, kuid) {}
    static constexpr EntityType _type = "most";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
};

class Scenery : public SceneryAbstract {
public:
    std::string name;
    Scenery(const int32_t& id, const Kuid& kuid) : SceneryAbstract(id, kuid) {}
    static constexpr EntityType _type = "mOBJ";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
};

class Camera : public Scenery {
public:
    WorldCoordinate coordinate2;
    Camera(const int32_t& id, const Kuid& kuid) : Scenery(id, kuid) {}
    static constexpr EntityType _type = "cmOb";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
};

class InterlockingTower : public Scenery {
public:
    InterlockingTower(const int32_t& id, const Kuid& kuid) : Scenery(id, kuid) {}
    static constexpr EntityType _type = "moit";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
};

#endif //TRK_VIEWER_SCENERY_H
