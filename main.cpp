#include <iostream>
#include <optional>
#include <SFML/Graphics.hpp>
#include <SFML/Window/Mouse.hpp>
#include <cmath>
#include <filesystem>
#include "World.h"
#include "vectorsHelper.h"

int main0(int argc, char **argv) {
    World world;
    auto path = std::filesystem::path(argc > 1 ? argv[1] : ".");
    std::cout << "read path: " << path << std::endl;
    world.loadMap(path, "mapfile");
    if (argc > 2) {
        auto path2 = std::filesystem::path(argv[2]);
        std::cout << "read path: " << path2 << std::endl;
        world.loadProfile(path2, "profile");
    }

    sf::RenderWindow window(sf::VideoMode(800, 800), "trk-viewer");
    window.setVerticalSyncEnabled(true);

    sf::RectangleShape someRect;
    someRect.setFillColor(sf::Color(255, 255, 255));
    someRect.setSize(sf::Vector2f(5.f, 5.f));

    auto hudView = window.getDefaultView();
    auto plotView = window.getDefaultView();

    auto screenSize = window.getSize();
    plotView.setSize(static_cast<float>(screenSize.y), -static_cast<float>(screenSize.x));
    plotView.rotate(90);

    std::optional<sf::Vector2f> moveBase = std::nullopt;

    sf::Font font;
    if (!font.loadFromFile("GOST2304_TypeA_italic.ttf")) {
        std::cerr << "can not load font" << std::endl;
        return EXIT_FAILURE;
    }

    while (window.isOpen()) {
        sf::Event event{};
        while (window.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed:
                    window.close();
                    break;
                case sf::Event::Resized:
                    {
                        auto oldSize = plotView.getSize();
                        plotView.setSize(oldSize.x * static_cast<float>(event.size.width) / static_cast<float>(screenSize.x),
                                         oldSize.y * static_cast<float>(event.size.height) / static_cast<float>(screenSize.y));
                    }
                    screenSize.x = event.size.width;
                    screenSize.y = event.size.height;
                    hudView.reset(sf::FloatRect(0.f, 0.f, static_cast<float>(event.size.width),static_cast<float>(event.size.height)));
                    break;
                case sf::Event::MouseWheelScrolled:
                    if (sf::Mouse::VerticalWheel == event.mouseWheelScroll.wheel) {
                        auto screenPosition = sf::Vector2i (event.mouseWheelScroll.x, event.mouseWheelScroll.y);
                        auto movedBaseTmp = moveBase.value_or(window.mapPixelToCoords(screenPosition, plotView));
                        if (event.mouseWheelScroll.delta > 0) {
                            plotView.zoom(1. / 1.1);
                        } else {
                            plotView.zoom(1.1);
                        }
                        plotView.move(movedBaseTmp - window.mapPixelToCoords(screenPosition, plotView));
                    }
                    break;
                case sf::Event::MouseButtonPressed:
                    if (event.mouseButton.button == sf::Mouse::Left) {
                        someRect.setPosition(window.mapPixelToCoords(sf::Vector2i(event.mouseButton.x, event.mouseButton.y), plotView));
                    }
                    if (sf::Mouse::Right == event.mouseButton.button) {
                        moveBase.emplace(window.mapPixelToCoords(sf::Vector2i(event.mouseButton.x, event.mouseButton.y), plotView));
                    }
                    break;
                case sf::Event::MouseButtonReleased:
                    if (sf::Mouse::Right == event.mouseButton.button) {
                        moveBase = std::nullopt;
                    }
                    break;
                case sf::Event::MouseMoved:
                    if (moveBase) {
                        sf::Vector2i screenPosition(event.mouseMove.x, event.mouseMove.y);
                        plotView.move(*moveBase - window.mapPixelToCoords(screenPosition, plotView));
                    }
                    break;
            }
        }

        window.clear();

        auto viewTileMin = window.mapPixelToCoords({0, 0}, plotView) / 720.f;
        auto viewTileMax = window.mapPixelToCoords(sf::Vector2i(screenSize), plotView) / 720.f;

        window.setView(plotView);
        world.setViewArea(sf::Vector2<int16_t>(viewTileMin) - sf::Vector2<int16_t>(2, 2), sf::Vector2<int16_t>(viewTileMax) + sf::Vector2<int16_t>(2, 2));
        window.draw(world);
        window.draw(someRect);

        window.setView(hudView);

        for (int i = std::ceil(viewTileMin.x); i < static_cast<int>(std::ceil(viewTileMax.x)); ++i) {
            sf::Vector2i x(window.mapCoordsToPixel(sf::Vector2f(static_cast<float>(720 * i), static_cast<float>(720 * i)), plotView));
            sf::Vertex line[] = {
                    sf::Vertex(sf::Vector2f(0.f, static_cast<float>(x.y)), sf::Color::Green),
                    sf::Vertex(sf::Vector2f(static_cast<float>(screenSize.x), static_cast<float>(x.y)), sf::Color::Green),
            };
            window.draw(line, 2, sf::Lines);

            sf::Text label(std::to_string(i), font);
            label.setFillColor(sf::Color::Green);
            label.setPosition(0.f, static_cast<float>(x.y));
            window.draw(label);
        }

        for (int i = std::ceil(viewTileMin.y); i < static_cast<int>(std::ceil(viewTileMax.y)); ++i) {
            sf::Vector2i x(window.mapCoordsToPixel(sf::Vector2f(static_cast<float>(720 * i), static_cast<float>(720 * i)), plotView));
            sf::Vertex line[] = {
                    sf::Vertex(sf::Vector2f(static_cast<float>(x.x), 0.f), sf::Color::Green),
                    sf::Vertex(sf::Vector2f(static_cast<float>(x.x), static_cast<float>(screenSize.y)), sf::Color::Green),
            };
            window.draw(line, 2, sf::Lines);

            sf::Text label(std::to_string(i), font);
            label.setFillColor(sf::Color::Green);
            label.setPosition(static_cast<float>(x.x), 0.f);
            window.draw(label);
        }
        auto cursorPosition = sf::Mouse::getPosition(window);
        auto worldPosition = window.mapPixelToCoords(cursorPosition, plotView);
        std::stringstream ss;
        auto cursorTile = sf::Vector2f(std::floor(worldPosition.x / 720), std::floor(worldPosition.y / 720));
        ss << cursorTile << " + ";
        ss << (worldPosition - 720.f * cursorTile);
        sf::Text txt(ss.str(), font);
        txt.setFillColor(sf::Color::Green);
        txt.setPosition(sf::Vector2f(cursorPosition));
        window.draw(txt);

        window.display();
    }

    return EXIT_SUCCESS;
}
