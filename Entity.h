#include "Kuid.h"
#include "EntityType.h"
#include "CoordinateTransformer.h"
#include <sstream>
#include <SFML/Graphics/Drawable.hpp>
#include <wx/dc.h>

#ifndef TRK_VIEWER_ENTITY_H
#define TRK_VIEWER_ENTITY_H


class Entity : public sf::Drawable {
public:
    int32_t _id;
    Kuid _kuid;
    const uint32_t _type = 0;
//    Entity() = default;
    Entity(const int32_t& id, const Kuid& kuid) : _id(id), _kuid(kuid) {};
    ~Entity() override = default;
    [[nodiscard]] virtual EntityType getType() const = 0;
    virtual void loadBody(std::istream& istream) = 0;
    virtual void saveBody(std::ostream& ostream) const = 0;
    void save(std::ostream& ostream) const;
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override {}
    virtual void render(wxDC& dc, const CoordinateTransformer& transformer) const {};
};


#endif //TRK_VIEWER_ENTITY_H
