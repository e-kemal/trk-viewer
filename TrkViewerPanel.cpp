#include <wx/dcclient.h>
#include <wx/dcbuffer.h>
#include "TrkViewerPanel.h"

BEGIN_EVENT_TABLE(TrkViewerPanel, wxPanel)
                EVT_ERASE_BACKGROUND(TrkViewerPanel::eraseBackground)
                EVT_PAINT(TrkViewerPanel::paintEvent)
                EVT_LEFT_DOWN(TrkViewerPanel::mouseLeftDown)
                EVT_LEFT_UP(TrkViewerPanel::mouseLeftUp)
                EVT_RIGHT_DOWN(TrkViewerPanel::mouseRightDown)
                EVT_RIGHT_UP(TrkViewerPanel::mouseRightUp)
                EVT_MOTION(TrkViewerPanel::mouseMoved)
                EVT_LEAVE_WINDOW(TrkViewerPanel::mouseLeftWindow)
                EVT_KEY_DOWN(TrkViewerPanel::keyPressed)
                EVT_KEY_UP(TrkViewerPanel::keyReleased)
                EVT_MOUSEWHEEL(TrkViewerPanel::mouseWheelMoved)
                EVT_SIZE(TrkViewerPanel::resize)
END_EVENT_TABLE()

void TrkViewerPanel::eraseBackground(wxEraseEvent& event) {
}

void TrkViewerPanel::paintEvent(wxPaintEvent &event) {
    wxBufferedPaintDC dc(this);
//    wxPaintDC dc(this);
    render(dc);
}

void TrkViewerPanel::paintNow() {
    wxWindow::Refresh(false);
//    wxClientDC dc(this);
//    render(dc);
}

void TrkViewerPanel::render(wxDC& dc) {
    dc.SetBackground(*wxBLACK_BRUSH);
    dc.Clear();

    int16_t widthTiles = std::ceil((static_cast<float>(GetSize().x) / zoom + viewOffset.m_x) / 720.f);
    int16_t heightTiles = std::ceil((static_cast<float>(GetSize().y) / zoom + viewOffset.m_y) / 720.f);

    world.render(dc, {viewTile, {widthTiles, heightTiles}, viewOffset, zoom});

    dc.SetPen(*wxGREEN_PEN);
//    dc.SetFont(wxFont(25, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_ITALIC, wxFONTWEIGHT_NORMAL));
    dc.SetTextForeground(*wxGREEN);

    for (int i = 0; i < widthTiles; ++i) {
        auto x = static_cast<wxCoord>((720.f * static_cast<float>(i) - viewOffset.m_x) * zoom);
        if (x < -1) {
            continue;
        }
        dc.DrawLine(x, 0, x, GetSize().y);
        dc.DrawText(std::to_string(viewTile.m_x + i), x, 0);
        dc.DrawText(std::to_string(viewTile.m_x + i), x, 15);
    }

    for (int i = 0; i < heightTiles; ++i) {
        auto y = static_cast<wxCoord>((720.f * static_cast<float>(i) - viewOffset.m_y) * zoom);
        if (y < -1) {
            continue;
        }
        dc.DrawLine(0, y, GetSize().x, y);
        dc.DrawText(std::to_string(viewTile.m_y + i), 0, y);
        dc.DrawText(std::to_string(viewTile.m_y + i), 0, y + 15);
    }

    if (selecting) {
        dc.SetPen(*wxBLUE_PEN);
        dc.SetBrush({wxColor{0, 0, 255, 127}});
        dc.DrawRectangle({selectStart, selectEnd});
    }
}

void TrkViewerPanel::mouseLeftDown(wxMouseEvent& event) {
    if (event.GetModifiers() & (wxMOD_CONTROL | wxMOD_RAW_CONTROL)) {
        wxPoint2DDouble mousePosition = event.GetPosition();
        moveBaseTile = viewTile;
        moveBaseOffset = mousePosition / zoom + viewOffset;
        moving = true;
    }
    else {
        selectStart = event.GetPosition();
        selecting = true;
    }
}

void TrkViewerPanel::mouseLeftUp(wxMouseEvent& event) {
    moving = false;
    selecting = false;
    paintNow();
}

void TrkViewerPanel::mouseRightDown(wxMouseEvent &event) {
//    wxPoint2DDouble mousePosition = event.GetPosition();
//    moveBaseTile = viewTile;
//    moveBaseOffset = mousePosition / zoom + viewOffset;
//    moving = true;
}

void TrkViewerPanel::mouseRightUp(wxMouseEvent &event) {
//    moving = false;
}

void TrkViewerPanel::mouseMoved(wxMouseEvent& event) {
    if (moving) {
        wxPoint2DDouble mousePosition = event.GetPosition();
        auto moveDstTile = viewTile;
        auto moveDstOffset = mousePosition / zoom + viewOffset;
        viewTile += moveBaseTile - moveDstTile;
        viewOffset += moveBaseOffset - moveDstOffset;

        wxPoint2DInt corr;
        (viewOffset / 720.).GetFloor(&corr.m_x, &corr.m_y);
        viewTile += corr;
        viewOffset -= corr * 720;
    }
    selectEnd = event.GetPosition();
    if (moving || selecting) {
        paintNow();
    }
}

void TrkViewerPanel::mouseWheelMoved(wxMouseEvent& event) {
    if (wxMOUSE_WHEEL_VERTICAL == event.GetWheelAxis()) {
        wxPoint2DDouble mousePosition = event.GetPosition();
        auto srcOffset = mousePosition / zoom;
        if (event.GetWheelRotation() > 0) {
            zoom *= 1.1;
        }
        else {
            zoom /= 1.1;
        }
        auto dstOffset = mousePosition / zoom;
        viewOffset += srcOffset - dstOffset;

        wxPoint2DInt corr;
        (viewOffset / 720.).GetFloor(&corr.m_x, &corr.m_y);
        viewTile += corr;
        viewOffset -= corr * 720;
        paintNow();
    }
}

void TrkViewerPanel::mouseLeftWindow(wxMouseEvent& event) {

}

void TrkViewerPanel::keyPressed(wxKeyEvent& event) {

}

void TrkViewerPanel::keyReleased(wxKeyEvent& event) {

}

void TrkViewerPanel::resize(wxSizeEvent &event) {

}
