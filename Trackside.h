#ifndef TRK_VIEWER_TRACKSIDE_H
#define TRK_VIEWER_TRACKSIDE_H


#include "Scenery.h"

class Trackside : public Scenery {
public:
    Trackside(const int32_t& id, const Kuid& kuid) : Scenery(id, kuid) {}
    static constexpr EntityType _type = "tsO2";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
};

class Junction : public Trackside {
public:
    Junction(const int32_t& id, const Kuid& kuid) : Trackside(id, kuid) {}
    static constexpr EntityType _type = "jnOb";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
};

class Signal : public Trackside {
public:
    Signal(const int32_t& id, const Kuid& kuid) : Trackside(id, kuid) {}
    static constexpr EntityType _type = "sgOb";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
};

class TrackCircuitDetector : public Trackside {
public:
    TrackCircuitDetector(const int32_t& id, const Kuid& kuid) : Trackside(id, kuid) {}
    static constexpr EntityType _type = "tcbD";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
};

class TrackCircuitInsulator : public Trackside {
public:
    TrackCircuitInsulator(const int32_t& id, const Kuid& kuid) : Trackside(id, kuid) {}
    static constexpr EntityType _type = "tcbI";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
};

class Vehicle : public Trackside {
public:
    Vehicle(const int32_t& id, const Kuid& kuid) : Trackside(id, kuid) {}
};


#endif //TRK_VIEWER_TRACKSIDE_H
