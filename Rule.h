#ifndef TRK_VIEWER_RULE_H
#define TRK_VIEWER_RULE_H


#include "Entity.h"
#include "WorldCoordinate.h"

class Rule : public Entity {
public:
    WorldCoordinate coordinate1;
    WorldCoordinate coordinate2;
    Rule(const int32_t& id, const Kuid& kuid) : Entity(id, kuid) {}
    static constexpr EntityType _type = "Rule";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
    void saveBody(std::ostream &ostream) const override;
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
    void render(wxDC& dc, const CoordinateTransformer& transformer) const override;
};


#endif //TRK_VIEWER_RULE_H
