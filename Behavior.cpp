#include <iomanip>
#include "Behavior.h"
#include "Trackside.h"

void Behavior::loadBody(std::istream &istream) {
    int32_t version;
    istream.read(reinterpret_cast<char*>(&version), 4);
std::cout << "\t\t\tversion(6): " << version << std::endl;
    if (version > 3) {
        int32_t a;
        char b, c, d;
        istream.read(reinterpret_cast<char*>(&a), 4);
        istream.read(&b, 1);
        istream.read(&c, 1);
        istream.read(&d, 1);
        std::cout << "\t\t\t??: " << a << ' ' << static_cast<int>(b) << ' ' << static_cast<int>(c) << ' ' << static_cast<int>(d) << ' ' << std::endl;
    }
    int32_t e;
    istream.read(reinterpret_cast<char*>(&e), 4);
    char f;
    istream.read(&f, 1);
    int32_t g;
    istream.read(reinterpret_cast<char*>(&g), 4);
std::cout << "\t\t\t??: " << ' ' << e << ' ' << static_cast<int>(f) << ' ' << g << std::endl;
    uint32_t len;
    istream.read(reinterpret_cast<char*>(&len), 4);
    name.resize(len);
    istream.read(name.data(), len);
    if ('\0' == name.back()) {
        name.erase(name.length() - 1);
    }
    int32_t h;
    istream.read(reinterpret_cast<char*>(&h), 4);
std::cout << "\t\t\tname: \"" << name << "\", level: " << h << std::endl;
    std::string i;
    i.resize(4);
    istream.read(reinterpret_cast<char*>(i.data()), 4);
    int32_t a1, a2;
    int32_t soupSize;
    istream.read(reinterpret_cast<char*>(&a1), 4);
    istream.read(reinterpret_cast<char*>(&a2), 4);
    istream.read(reinterpret_cast<char*>(&soupSize), 4);
std::cout << "\t\t\tsoup: " << i << ", " << a1 << ' ' << a2 << ", size: " << soupSize << std::endl;
    std::string soup;
    soup.resize(soupSize);
    istream.read(soup.data(), soupSize);
}

void Behavior::saveBody(std::ostream &ostream) const {

}

void Library::loadBody(std::istream &istream) {
    int32_t version;
    istream.read(reinterpret_cast<char*>(&version), 4);
    std::cout << "\t\t\tversion(0): " << version << std::endl;
    std::string i;
    i.resize(4);
    istream.read(reinterpret_cast<char*>(i.data()), 4);
    int32_t a1, a2;
    int32_t soupSize;
    istream.read(reinterpret_cast<char*>(&a1), 4);
    istream.read(reinterpret_cast<char*>(&a2), 4);
    istream.read(reinterpret_cast<char*>(&soupSize), 4);
    std::cout << "\t\t\tsoup: " << i << ", " << a1 << ' ' << a2 << ", size: " << soupSize << std::endl;
    std::string soup;
    soup.resize(soupSize);
    istream.read(soup.data(), soupSize);
}

void Library::saveBody(std::ostream &ostream) const {

}

void TrainConsist::loadBody(std::istream &istream) {
    uint32_t a;
    int32_t b, count;
    int32_t d, e;
    float f;
    int32_t g;
    istream.read(reinterpret_cast<char*>(&a), 4);
    istream.read(reinterpret_cast<char*>(&b), 4);
    istream.read(reinterpret_cast<char*>(&count), 4);
    istream.read(reinterpret_cast<char*>(&d), 4);
    istream.read(reinterpret_cast<char*>(&e), 4);
    istream.read(reinterpret_cast<char*>(&f), 4);
    istream.read(reinterpret_cast<char*>(&g), 4);
    std::cout << "\t??: " << std::hex << a << std::dec << ' ' << b << ", count: " << count << ", ??: " << d << ", track: " << e << ", distance: " << f << ", dir: " << g << std::endl;
    for (int i = 0; i < count; ++i) {
        Kuid kuid(istream);
        int32_t h;
        char j;
        int32_t k;
        istream.read(reinterpret_cast<char*>(&h), 4);
        istream.read(&j, 1);
        istream.read(reinterpret_cast<char*>(&k), 4);
        std::cout << "\tvehicle: " << i << ", kuid: " << kuid << ' ' << h << ' ' << static_cast<int>(j) << ", len: " << k << std::endl;
        std::string str;
        str.resize(k);
        istream.read(str.data(), k);
        std::istringstream buf(str);

        int32_t version;
        buf.read(reinterpret_cast<char*>(&version), 4);
        std::cout << "\t\tversion(11): " << version << std::endl;

        Vehicle vehicle(0, Kuid());
        vehicle.loadBody(buf);

        std::cout << "\t\tdata:";
        char tt = 0;
        buf.read(&tt, 1);
        while (!buf.eof()) {
            std::cout << ' ' << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(static_cast<uint8_t>(tt));
            buf.read(&tt, 1);
        }
        std::cout << std::dec << std::endl;
    }
}

void TrainConsist::saveBody(std::ostream &ostream) const {

}
