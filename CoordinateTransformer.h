#ifndef TRK_VIEWER_COORDINATETRANSFORMER_H
#define TRK_VIEWER_COORDINATETRANSFORMER_H


#include <wx/geometry.h>
#include "WorldCoordinate.h"

class CoordinateTransformer {
    wxPoint2DInt _tile;
    wxPoint2DInt _sizeTiles;
    wxPoint2DDouble _offset;
    double _zoom;
public:
    CoordinateTransformer(const wxPoint2DInt &tile, const wxPoint2DInt &sizeTiles, const wxPoint2DDouble &offset, double zoom) : _tile(tile), _sizeTiles(sizeTiles), _offset(offset), _zoom(zoom) {}
    bool isTileInViewport(const wxPoint2DInt& tile) const;
    wxPoint operator()(const WorldCoordinate& coordinate) const;
    wxPoint operator()(const wxPoint2DInt& tile, const wxPoint2DDouble& offset) const;
    wxPoint operator()(const sf::Vector2<int16_t>& tile, const sf::Vector3f& offset) const;
};


#endif //TRK_VIEWER_COORDINATETRANSFORMER_H
