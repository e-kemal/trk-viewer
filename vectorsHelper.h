#ifndef TRK_VIEWER_VECTORSHELPER_H
#define TRK_VIEWER_VECTORSHELPER_H

#include <ostream>
#include <SFML/System/Vector2.hpp>
#include <SFML/System/Vector3.hpp>
#include "WorldCoordinate.h"

template <typename T>
inline std::ostream& operator << (std::ostream& ostream, sf::Vector2<T> vector2) {
    ostream << '(' << vector2.x << ", " << vector2.y << ')';
    return ostream;
}

template <typename T>
inline std::ostream& operator << (std::ostream& ostream, sf::Vector3<T> vector3) {
    ostream << '(' << vector3.x << ", " << vector3.y << ", " << vector3.z << ')';
    return ostream;
}

template <typename T>
inline sf::Vector2<T> to2d(const sf::Vector3<T>& v) {
    return {v.x, v.y};
}

inline sf::Vector2f to2d(const WorldCoordinate& coordinate) {
    return {static_cast<float>(720 * coordinate._baseboard.x) + coordinate._offset.x, static_cast<float>(720 * coordinate._baseboard.y) + coordinate._offset.y};
}

#endif //TRK_VIEWER_VECTORSHELPER_H
