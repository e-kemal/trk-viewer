#ifndef TRK_VIEWER_FIXEDTRACK_H
#define TRK_VIEWER_FIXEDTRACK_H


#include "Scenery.h"

class FixedTrack : public Scenery {
public:
    FixedTrack(const int32_t& id, const Kuid& kuid) : Scenery(id, kuid) {}
    static constexpr EntityType _type = "mFXT";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
};

class Crossing : public FixedTrack {
public:
    Crossing(const int32_t& id, const Kuid& kuid) : FixedTrack(id, kuid) {}
    static constexpr EntityType _type = "obCr";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
};

class Turntable : public FixedTrack {
public:
    Turntable(const int32_t& id, const Kuid& kuid) : FixedTrack(id, kuid) {}
    static constexpr EntityType _type = "ttOb";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
};

class Buildable : public FixedTrack {
public:
    Buildable(const int32_t& id, const Kuid& kuid) : FixedTrack(id, kuid) {}
    static constexpr EntityType _type = "mBUI";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
};

class Industry : public Buildable {
public:
    Industry(const int32_t& id, const Kuid& kuid) : Buildable(id, kuid) {}
    static constexpr EntityType _type = "mIND";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
};

#endif //TRK_VIEWER_FIXEDTRACK_H
