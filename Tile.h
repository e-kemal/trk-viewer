#ifndef TRK_VIEWER_TILE_H
#define TRK_VIEWER_TILE_H


#include <map>
#include <memory>
#include <wx/dc.h>
#include "Entity.h"

class Tile : public sf::Drawable {
public:
    std::map<int32_t, std::weak_ptr<Entity>> vertexes;
    std::map<int32_t, std::weak_ptr<Entity>> tracks;
    std::map<int32_t, std::weak_ptr<Entity>> rules;
    std::map<int32_t, std::weak_ptr<Entity>> objects;
    ~Tile() override = default;
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
    void render(wxDC& dc, const CoordinateTransformer& transformer);
};


#endif //TRK_VIEWER_TILE_H
