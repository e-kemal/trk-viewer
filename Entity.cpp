#include "Entity.h"

void Entity::save(std::ostream &ostream) const {
    std::ostringstream buf;
    saveBody(buf);
    auto str = buf.str();
    ostream.write(reinterpret_cast<const char*>(&_id), 4);
    auto type = getType();
    ostream.write(reinterpret_cast<const char*>(&type), 4);
    _kuid.save(ostream);
    auto len = str.size();
    ostream.write(reinterpret_cast<const char*>(&len), 4);
    ostream.write(str.data(), len);
}
