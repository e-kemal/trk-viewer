#include <iomanip>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include "TrackVertex.h"

void TrackVertex::loadBody(std::istream &istream) {
    int version;
    istream.read(reinterpret_cast<char*>(&version), 4);
std::cout << "\t\t\tversion (7): " << version << std::endl;
    coordinate.load(istream);
std::cout << "\t\t\tcoordinate: " << coordinate << std::endl;
    for (int j = 0; j < 4; ++j) {
        istream.read(reinterpret_cast<char*>(&track_id[j]), 4);
        char s;
        istream.read(reinterpret_cast<char*>(&s), 1);
std::cout << "\t\t\t\ttrack" << j << ": " << track_id[j] << ' ' << static_cast<int>(s);
        if (version > 3) {
            int t = 0;
            istream.read(reinterpret_cast<char*>(&t), 1);
std::cout << ", " << t;

            BoundaryBox box(istream);
std::cout << "\t\tbox: " << box;
        }
        std::cout << std::endl;
    }
    istream.read(reinterpret_cast<char*>(&dir), 1);
std::cout << "\t\t\tdir: " << static_cast<int>(dir) << std::endl;
if (0 != dir && 1 != dir) {std::cerr << "id: " << _id << ", dir: " << static_cast<int>(dir) << std::endl;}
    if (version < 4) {
        return;
    }
    istream.read(reinterpret_cast<char*>(&superElevationDegree), 4);
    istream.read(reinterpret_cast<char*>(&superElevationLimit), 4);
std::cout << "\t\t\tsuperElevation: " << superElevationDegree << ' ' << superElevationLimit << std::endl;

    char t = 0;
    istream.read(&t, 1);
    if (t) {
        istream.read(reinterpret_cast<char*>(&scenery_id1), 4);
        istream.read(reinterpret_cast<char*>(&scenery_id2), 4);
std::cout << "\t\t\tin scenery(" << static_cast<int>(t) << "): " << scenery_id1 << ' ' << scenery_id2;
        if (scenery_id1 != scenery_id2 && scenery_id2 >= 0) {
            istream.read(reinterpret_cast<char*>(&scenery_id2), 4);
std::cout << ' ' << scenery_id2;
        }
std::cout << std::endl;
    }
    istream.read(reinterpret_cast<char*>(&flags), 4);
std::cout << "\t\t\tflags: " << std::hex << std::setfill('0') << std::setw(8) << flags << std::dec;
    auto flags2 = flags;
    if (flags & 0x1) {
        flags2 &= ~0x1;
std::cout << " visible";//not scenery inside
    }
    if (flags & 0x2) {
        flags2 &= ~0x2;
std::cout << " is_attached";
    }
    if (flags & 0x4) {
        flags2 &= ~0x4;
std::cout << " is_bridge";
    }
    if (flags & 0x10) {
        flags2 &= ~0x10;
std::cout << " is_track";
    }
    flags2 ^= 0x00000008;
std::cout << ' ' << std::hex << std::setfill('0') << std::setw(8) << flags2 << std::dec << std::endl;
    if (flags2) {
std::cerr << "vertex: " << _id << ", flags: " << std::hex << std::setfill('0') << std::setw(8) << flags2 << std::dec << std::endl;
    }
}

void TrackVertex::saveBody(std::ostream &ostream) const {

}

void TrackVertex::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    sf::CircleShape circle{.5f};
    circle.setPosition(static_cast<float>(720 * coordinate._baseboard.x) + coordinate._offset.x - .5f, static_cast<float>(720 * coordinate._baseboard.y) + coordinate._offset.y - .5f);
    circle.setFillColor(sf::Color::Blue);
    target.draw(circle, states);
}

void TrackVertex::render(wxDC &dc, const CoordinateTransformer &transformer) const {
    dc.SetPen(*wxBLUE_PEN);
    dc.SetBrush(*wxBLUE_BRUSH);
    dc.DrawCircle(transformer(coordinate), 3);
}
