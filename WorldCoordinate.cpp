#include "WorldCoordinate.h"
#include "vectorsHelper.h"

WorldCoordinate::WorldCoordinate(std::istream &istream) : WorldCoordinate() {
    load(istream);
}

void WorldCoordinate::load(std::istream& istream) {
    istream.read(reinterpret_cast<char*>(&_baseboard.x), 2);
    istream.read(reinterpret_cast<char*>(&_baseboard.y), 2);
    istream.read(reinterpret_cast<char*>(&_offset.x), 4);
    istream.read(reinterpret_cast<char*>(&_offset.y), 4);
    istream.read(reinterpret_cast<char*>(&_offset.z), 4);
}

void WorldCoordinate::save(std::ostream& ostream) const {
    ostream.write(reinterpret_cast<const char*>(&_baseboard.x), 2);
    ostream.write(reinterpret_cast<const char*>(&_baseboard.y), 2);
    ostream.write(reinterpret_cast<const char*>(&_offset.x), 4);
    ostream.write(reinterpret_cast<const char*>(&_offset.y), 4);
    ostream.write(reinterpret_cast<const char*>(&_offset.z), 4);
}

std::ostream& operator<< (std::ostream& ostream, const WorldCoordinate& coordinate)
{
    ostream << coordinate._baseboard << " + " << coordinate._offset;
    return ostream;
}
