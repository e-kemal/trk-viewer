#include <fstream>
#include <SFML/System/Vector2.hpp>

#ifndef TRK_VIEWER_BOUNDARYBOX_H
#define TRK_VIEWER_BOUNDARYBOX_H


class BoundaryBox {
public:
    sf::Vector2<int16_t> _baseboard;
    float _x_min{};
    float _x_max{};
    float _y_min{};
    float _y_max{};
    float _z_min{};
    float _z_max{};
    BoundaryBox() = default;
    explicit BoundaryBox(std::istream& istream);
    void load(std::istream& istream);
    void save(std::ostream& ostream) const;
};

std::ostream& operator<< (std::ostream& ostream, const BoundaryBox& box);

#endif //TRK_VIEWER_BOUNDARYBOX_H
