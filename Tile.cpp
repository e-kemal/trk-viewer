#include "Tile.h"

void Tile::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    for (const auto& [id, trk] : tracks) {
        if (auto t = trk.lock()) {
            t->draw(target, states);
        }
    }
    for (const auto& [id, vtx] : vertexes) {
        if (auto v = vtx.lock()) {
            v->draw(target, states);
        }
    }
    for (const auto& [id, rule] : rules) {
        if (auto r = rule.lock()) {
            r->draw(target, states);
        }
    }
    for (const auto& [id, object] : objects) {
        if (auto o = object.lock()) {
            o->draw(target, states);
        }
    }
}

void Tile::render(wxDC& dc, const CoordinateTransformer& transformer) {
    for (const auto& [id, object] : objects) {
        if (auto o = object.lock()) {
            o->render(dc, transformer);
        }
    }
    for (const auto& [id, track] : tracks) {
        if (auto t = track.lock()) {
            t->render(dc, transformer);
        }
    }
    for (const auto& [id, vertex] : vertexes) {
        if (auto v = vertex.lock()) {
            v->render(dc, transformer);
        }
    }
    for (const auto& [id, rule] : rules) {
        if (auto r = rule.lock()) {
            r->render(dc, transformer);
        }
    }
}
