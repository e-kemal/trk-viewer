#include <fstream>
#include "World.h"
#include "vectorsHelper.h"
#include "TrackVertex.h"
#include "BridgeSection.h"
#include "Rule.h"
#include "Trackside.h"
#include "FixedTrack.h"
#include "Behavior.h"
#include <SFML/Graphics/RenderTarget.hpp>

bool World::loadEntities(std::istream& tile, const std::optional<sf::Vector2<int16_t>>& tilePosition) {
    int32_t id;
    tile.read(reinterpret_cast<char*>(&id), 4);
    while (id >= 0) {
        uint32_t t;
        tile.read(reinterpret_cast<char*>(&t), 4);
        Kuid kuid(tile);
        uint32_t len;
        tile.read(reinterpret_cast<char*>(&len), 4);
        auto type = EntityType(t);
        std::cout << "\t\tentity type: " << type << ", id: " << id << ", kuid: " << kuid << ", len: " << len << std::endl;
        std::string str;
        str.resize(len);
        tile.read(str.data(), len);
        std::istringstream buf(str);
        if (TrackVertex::_type == type) {
            auto trackVertex = std::make_shared<TrackVertex>(id, kuid);
            trackVertex->loadBody(buf);
            auto it = allVtx.find(id);
            if (allVtx.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allVtx.erase(it);
                for (auto& item : tiles) {
                    item.second.vertexes.erase(id);
                }
            }
            allVtx.insert({id, trackVertex});
            tiles[trackVertex->coordinate._baseboard].vertexes.insert({id, trackVertex});
            if (tilePosition && trackVertex->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << ' ' << trackVertex->coordinate._baseboard << std::endl;
            }
        }
        else if (TrackSection::_type == type) {
            auto trackSection = std::make_shared<TrackSection>(id, kuid);
            trackSection->loadBody(buf);
            auto it = allTrk.find(id);
            if (allTrk.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allTrk.erase(it);
                for (auto& item : tiles) {
                    item.second.tracks.erase(id);
                }
            }
            allTrk.insert({id, trackSection});
            tiles[trackSection->inTile].tracks.insert({id, trackSection});
            if (tilePosition && trackSection->inTile != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << trackSection->inTile << std::endl;
            }
            if (tilePosition && trackSection->box._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << trackSection->box._baseboard << std::endl;
            }
            if (trackSection->box._baseboard != trackSection->inTile) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << trackSection->inTile << trackSection->box._baseboard << std::endl;
            }
        }
        else if (BridgeSection::_type == type) {
            auto bridgeSection = std::make_shared<BridgeSection>(id, kuid);
            bridgeSection->loadBody(buf);
            auto it = allTrk.find(id);
            if (allTrk.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allTrk.erase(it);
                for (auto& item : tiles) {
                    item.second.tracks.erase(id);
                }
            }
            allTrk.insert({id, bridgeSection});
            tiles[bridgeSection->inTile].tracks.insert({id, bridgeSection});
            if (tilePosition && bridgeSection->inTile != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << bridgeSection->inTile << std::endl;
            }
            if (tilePosition && bridgeSection->box._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << bridgeSection->box._baseboard << std::endl;
            }
            if (bridgeSection->box._baseboard != bridgeSection->inTile) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << bridgeSection->inTile << bridgeSection->box._baseboard << std::endl;
            }
        }
        else if (Rule::_type == type) {
            auto rule = std::make_shared<Rule>(id, kuid);
            rule->loadBody(buf);
            auto it = allRules.find(id);
            if (allRules.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allRules.erase(it);
                for (auto& item : tiles) {
                    item.second.rules.erase(id);
                }
            }
            allRules.insert({id, rule});
            tiles[rule->coordinate1._baseboard].rules.insert({id, rule});
            if (tilePosition && rule->coordinate1._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << rule->coordinate1._baseboard << std::endl;
            }
            if (tilePosition && rule->coordinate2._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << rule->coordinate2._baseboard << std::endl;
            }
        }
        else if (SceneryBase::_type == type) {
            auto scenery = std::make_shared<SceneryBase>(id, kuid);
            scenery->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, scenery});
            tiles[scenery->coordinate._baseboard].objects.insert({id, scenery});
            if (tilePosition && scenery->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << scenery->coordinate._baseboard << std::endl;
            }
        }
        else if (ScenerySpeedTree::_type == type) {
            auto scenery = std::make_shared<ScenerySpeedTree>(id, kuid);
            scenery->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, scenery});
            tiles[scenery->coordinate._baseboard].objects.insert({id, scenery});
            if (tilePosition && scenery->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << scenery->coordinate._baseboard << std::endl;
            }
        }
        else if (Scenery::_type == type) {
            auto scenery = std::make_shared<Scenery>(id, kuid);
            scenery->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, scenery});
            tiles[scenery->coordinate._baseboard].objects.insert({id, scenery});
            if (tilePosition && scenery->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << scenery->coordinate._baseboard << std::endl;
            }
        }
        else if (Camera::_type == type) {
            auto camera = std::make_shared<Camera>(id, kuid);
            camera->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, camera});
            tiles[camera->coordinate._baseboard].objects.insert({id, camera});
            if (tilePosition && camera->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << camera->coordinate._baseboard << std::endl;
            }
        }
        else if (InterlockingTower::_type == type) {
            auto interlockingTower = std::make_shared<InterlockingTower>(id, kuid);
            interlockingTower->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, interlockingTower});
            tiles[interlockingTower->coordinate._baseboard].objects.insert({id, interlockingTower});
            if (tilePosition && interlockingTower->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << interlockingTower->coordinate._baseboard << std::endl;
            }
        }
        else if (FixedTrack::_type == type) {
            auto fixedTrack = std::make_shared<FixedTrack>(id, kuid);
            fixedTrack->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, fixedTrack});
            tiles[fixedTrack->coordinate._baseboard].objects.insert({id, fixedTrack});
            if (tilePosition && fixedTrack->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << fixedTrack->coordinate._baseboard << std::endl;
            }
        }
        else if (Turntable::_type == type) {
            auto turntable = std::make_shared<Turntable>(id, kuid);
            turntable->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, turntable});
            tiles[turntable->coordinate._baseboard].objects.insert({id, turntable});
            if (tilePosition && turntable->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << turntable->coordinate._baseboard << std::endl;
            }
        }
        else if (Crossing::_type == type) {
            auto crossing = std::make_shared<Crossing>(id, kuid);
            crossing->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, crossing});
            tiles[crossing->coordinate._baseboard].objects.insert({id, crossing});
            if (tilePosition && crossing->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << crossing->coordinate._baseboard << std::endl;
            }
        }
        else if (Buildable::_type == type) {
            auto buildable = std::make_shared<Buildable>(id, kuid);
            buildable->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, buildable});
            tiles[buildable->coordinate._baseboard].objects.insert({id, buildable});
            if (tilePosition && buildable->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << buildable->coordinate._baseboard << std::endl;
            }
        }
        else if (Industry::_type == type) {
            auto industry = std::make_shared<Industry>(id, kuid);
            industry->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, industry});
            tiles[industry->coordinate._baseboard].objects.insert({id, industry});
            if (tilePosition && industry->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << industry->coordinate._baseboard << std::endl;
            }
        }
        else if (Trackside::_type == type) {
            auto trackside = std::make_shared<Trackside>(id, kuid);
            trackside->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, trackside});
            tiles[trackside->coordinate._baseboard].objects.insert({id, trackside});
            if (tilePosition && trackside->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << trackside->coordinate._baseboard << std::endl;
            }
        }
        else if (Junction::_type == type) {
            auto junction = std::make_shared<Junction>(id, kuid);
            junction->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, junction});
            tiles[junction->coordinate._baseboard].objects.insert({id, junction});
            if (tilePosition && junction->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << junction->coordinate._baseboard << std::endl;
            }
        }
        else if (Signal::_type == type) {
            auto signal = std::make_shared<Signal>(id, kuid);
            signal->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, signal});
            tiles[signal->coordinate._baseboard].objects.insert({id, signal});
            if (tilePosition && signal->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << signal->coordinate._baseboard << std::endl;
            }
        }
        else if (TrackCircuitDetector::_type == type) {
            auto detector = std::make_shared<TrackCircuitDetector>(id, kuid);
            detector->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, detector});
            tiles[detector->coordinate._baseboard].objects.insert({id, detector});
            if (tilePosition && detector->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << detector->coordinate._baseboard << std::endl;
            }
        }
        else if (TrackCircuitInsulator::_type == type) {
            auto insulator = std::make_shared<TrackCircuitInsulator>(id, kuid);
            insulator->loadBody(buf);
            auto it = allObjects.find(id);
            if (allObjects.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allObjects.erase(it);
                for (auto& item : tiles) {
                    item.second.objects.erase(id);
                }
            }
            allObjects.insert({id, insulator});
            tiles[insulator->coordinate._baseboard].objects.insert({id, insulator});
            if (tilePosition && insulator->coordinate._baseboard != *tilePosition) {
                std::cerr << "type: " << type << ", id: " << id << ", wrong tile " << *tilePosition << insulator->coordinate._baseboard << std::endl;
            }
        }
        else if (Behavior::_type == type) {
            auto behavior = std::make_shared<Behavior>(id, kuid);
            behavior->loadBody(buf);
        }
        else if (Library::_type == type) {
            auto library = std::make_shared<Library>(id, kuid);
            if (len) {
                library->loadBody(buf);
            }
        }
        else {
            std::cerr << "unknown type " << type << std::endl;
        }

        std::cout << "\t\t\tdata:";
        char tt = 0;
        buf.read(&tt, 1);
        while (!buf.eof()) {
            std::cout << ' ' << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(static_cast<uint8_t>(tt));
            buf.read(&tt, 1);
        }
        std::cout << std::dec << std::endl;

        tile.read(reinterpret_cast<char*>(&id), 4);
    }
    std::cout << "\t\t-1: " << id << std::endl;
    return true;
}

bool World::loadEntitiesLegacy(std::istream &tile) {
    int32_t id;
    tile.read(reinterpret_cast<char*>(&id), 4);
    while (id >= 0) {
        uint32_t t;
        tile.read(reinterpret_cast<char*>(&t), 4);
//        Kuid kuid(tile);
//        uint32_t len;
//        tile.read(reinterpret_cast<char*>(&len), 4);
        auto type = EntityType(t);
        std::cout << "\t\tentity type: " << type << ", id: " << id /*<< ", kuid: " << kuid << ", len: " << len*/ << std::endl;
//        std::string str;
//        str.resize(len);
//        tile.read(str.data(), len);
//        std::istringstream buf(str);

        if (TrackVertex::_type == type) {
            auto trackVertex = std::make_shared<TrackVertex>(id, Kuid{});
            trackVertex->loadBody(tile);
            auto it = allVtx.find(id);
            if (allVtx.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allVtx.erase(it);
                for (auto& item : tiles) {
                    item.second.vertexes.erase(id);
                }
            }
            allVtx.insert({id, trackVertex});
            tiles[trackVertex->coordinate._baseboard].vertexes.insert({id, trackVertex});
        }
        else if (TrackSection::_type == type) {
            auto trackSection = std::make_shared<TrackSection>(id, Kuid{});
            trackSection->loadBody(tile);
            auto it = allTrk.find(id);
            if (allTrk.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allTrk.erase(it);
                for (auto& item : tiles) {
                    item.second.tracks.erase(id);
                }
            }
            allTrk.insert({id, trackSection});
            tiles[trackSection->inTile].tracks.insert({id, trackSection});

//            std::cout << "\t\t\tdata:";
//            char tt = 0;
//            for (int i = 0; i < 24; ++i) {
//                tile.read(&tt, 1);
//                std::cout << ' ' << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(static_cast<uint8_t>(tt));
//            }
//            std::cout << std::dec << std::endl;
        }
        else if (BridgeSection::_type == type) {
            auto bridgeSection = std::make_shared<BridgeSection>(id, Kuid{});
            bridgeSection->loadBody(tile);
            auto it = allTrk.find(id);
            if (allTrk.end() != it) {
                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
                allTrk.erase(it);
                for (auto& item : tiles) {
                    item.second.tracks.erase(id);
                }
            }
            allTrk.insert({id, bridgeSection});
            tiles[bridgeSection->inTile].tracks.insert({id, bridgeSection});
        }
//        else if (Signal::_type == type) {
//            auto signal = std::make_shared<Signal>(id, Kuid{});
//            signal->loadBody(tile);
//            auto it = allObjects.find(id);
//            if (allObjects.end() != it) {
//                std::cerr << "type: " << type << ", id: " << id << ", already added" << std::endl;
//                allObjects.erase(it);
//                for (auto& item : tiles) {
//                    item.second.objects.erase(id);
//                }
//            }
//            allObjects.insert({id, signal});
//            tiles[signal->coordinate._baseboard].objects.insert({id, signal});
//return true;
//        }
        else {
            std::cout << "unknown type" << std::endl;
            return false;
        }

        tile.read(reinterpret_cast<char*>(&id), 4);
    }
    std::cout << "\t\t-1: " << id << std::endl;
    return true;
}

bool World::loadEntityProperties(std::istream& tile, int32_t version) {
    int32_t id;
    tile.read(reinterpret_cast<char*>(&id), 4);
    while (id >= 0) {
        uint32_t t;
        tile.read(reinterpret_cast<char*>(&t), 4);
        uint32_t len;
        tile.read(reinterpret_cast<char*>(&len), 4);
        auto type = EntityType(t);
std::cout << "\t\tentity type: " << type << ", id: " << id << ", len: " << len;
        if (version > 7) {
            int32_t a;
            tile.read(reinterpret_cast<char*>(&a), 4);
std::cout << ", ??: " << a;
        }
std::cout << std::endl;
        std::string str;
        str.resize(len);
        tile.read(str.data(), len);
        std::istringstream buf(str);

        int32_t c, d;
        char e, f;
        buf.read(reinterpret_cast<char*>(&c), 4);
        buf.read(reinterpret_cast<char*>(&d), 4);
        buf.read(&e, 1);
        buf.read(&f, 1);
std::cout << "\t\t\t??: " << c << ' ' << d << ' ' << static_cast<int>(e) << ", useSoup: " << static_cast<int>(f) << std::endl;
        if (f) {
            std::string g;
            g.resize(4);
            buf.read(reinterpret_cast<char*>(g.data()), 4);
            int32_t a1, a2, soupSize;
            buf.read(reinterpret_cast<char*>(&a1), 4);
            buf.read(reinterpret_cast<char*>(&a2), 4);
            buf.read(reinterpret_cast<char*>(&soupSize), 4);
std::cout << "\t\t\tsoup: " << g << ", " << a1 << ' ' << a2 << ", size: " << soupSize << std::endl;
            std::string soup;
            soup.resize(soupSize);
            buf.read(soup.data(), soupSize);
        }
        int32_t cnt;
        buf.read(reinterpret_cast<char*>(&cnt), 4);
std::cout << "\t\t\tcount: " << cnt << std::endl;
        if (cnt) {
            for (int i = 0; i < cnt; ++i) {
                buf.read(reinterpret_cast<char *>(&c), 4);
                std::cout << "\t\t\t\t?\?(100): " << c;
                buf.read(reinterpret_cast<char *>(&c), 4);
                std::cout << ", (0): " << c;
                buf.read(reinterpret_cast<char *>(&c), 4);
                std::cout << ", capacity: " << c;
                buf.read(reinterpret_cast<char *>(&c), 4);
                std::cout << ", start amount: " << c;
                buf.read(reinterpret_cast<char *>(&c), 4);
                std::cout << ", (1)??: " << c << std::endl;
                if (c < 0 || c > 100) return false;
                for (int j = 0; j < c; ++j) {
                    Kuid kuid(buf);
                    int32_t amount, index;
                    buf.read(reinterpret_cast<char *>(&amount), 4);
                    buf.read(reinterpret_cast<char *>(&index), 4);
                    std::cout << "\t\t\t\t\tproduct: " << kuid << ", amount: " << amount << ", start index: " << index << std::endl;
                }
                int32_t a;
                buf.read(reinterpret_cast<char *>(&a), 4);
                std::cout << "\t\t\t\t\tdata: " << a;
                if (a < 0 || a > 1000) return false;
                for (int k = 0; k < a; ++k) {
                    char tt;
                    buf.read(&tt, 1);
                    std::cout << ' ' << static_cast<int>(static_cast<uint8_t>(tt));
                }
                std::cout << std::endl;

                buf.read(reinterpret_cast<char *>(&c), 4);
                Kuid kuid(buf);
                buf.read(reinterpret_cast<char *>(&d), 4);
                std::cout << "\t\t\t\t\t?\?(0): " << c << ", category: " << kuid << ", (0): " << d;
                buf.read(reinterpret_cast<char *>(&c), 4);
                std::cout << ", allowed-products(" << c << "):";
                for (int j = 0; j < c; ++j) {
                    kuid.load(buf);
                    std::cout << (j ? ',' : ' ') << kuid;
                }
                buf.read(reinterpret_cast<char *>(&c), 4);
                std::cout << ", allowed-categories(" << c << "):";
                for (int j = 0; j < c; ++j) {
                    kuid.load(buf);
                    std::cout << (j ? ',' : ' ') << kuid;
                }
                std::cout << std::endl;
            }
            buf.read(reinterpret_cast<char*>(&c), 4);
            std::cout << "\t\t\t??: " << c << std::endl;
            int32_t processCount;
            buf.read(reinterpret_cast<char*>(&processCount), 4);
            std::cout << "\t\t\tcount: " << processCount << std::endl;
            for (int i = 0; i < processCount; ++i) {
                buf.read(reinterpret_cast<char*>(&e), 1);
                std::cout << "\t\t\t\tenabled: " << static_cast<int>(e) << std::endl;
                buf.read(reinterpret_cast<char*>(&c), 4);
                std::cout << "\t\t\t\tinputs: " << c << std::endl;
                for (int j = 0; j < c; ++j) {
                    buf.read(reinterpret_cast<char*>(&d), 4);
                    std::cout << "\t\t\t\t\tamount: " << d;
                    buf.read(reinterpret_cast<char*>(&d), 4);
                    std::cout << ", queue: " << d;
                    Kuid kuid(buf);
                    std::cout << ", ??: " << kuid << std::endl;
                }
                buf.read(reinterpret_cast<char*>(&c), 4);
                std::cout << "\t\t\t\toutputs: " << c << std::endl;
                for (int j = 0; j < c; ++j) {
                    buf.read(reinterpret_cast<char*>(&d), 4);
                    std::cout << "\t\t\t\t\tamount: " << d;
                    buf.read(reinterpret_cast<char*>(&d), 4);
                    std::cout << ", queue: " << d;
                    Kuid kuid(buf);
                    std::cout << ", ??: " << kuid << std::endl;
                }
                float g;
                buf.read(reinterpret_cast<char*>(&g), 4);
                std::cout << "\t\t\t\tduration: " << g << std::endl;
            }
        }

std::cout << "\t\t\tdata:";
        char tt = 0;
        buf.read(&tt, 1);
        while (!buf.eof()) {
std::cout << ' ' << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(static_cast<uint8_t>(tt));
            buf.read(&tt, 1);
        }
std::cout << std::dec << std::endl;

        tile.read(reinterpret_cast<char*>(&id), 4);
    }
std::cout << "\t\t-1: " << id << std::endl;

    return true;
}

bool World::loadFile(std::istream& tile, int32_t fileType, const std::optional<sf::Vector2<int16_t>>& tilePosition) {
    int32_t tileVersion;
    tile.read(reinterpret_cast<char*>(&tileVersion), 4);
    if (tileVersion > 4) {
        tile.read(reinterpret_cast<char *>(&fileType), 4);
    }
    std::cout << "\t\tversion (8): " << tileVersion << ", type: " << fileType << std::endl;

    if (tileVersion > 2) {
        loadEntities(tile, tilePosition);
    }
    else {
        loadEntitiesLegacy(tile);
    }
//    std::cout << tile.tellg() << std::endl;
    if (tileVersion < 3) {
        return true;
    }
    if (tileVersion < 6) {
        int32_t a;
        tile.read(reinterpret_cast<char*>(&a), 4);
        std::cout << "\t\t??: " << a << ' ' << std::hex;
        for (int i = 0; i < a; ++i) {
            char t;
            tile.read(&t, 1);
            std::cout << ' ' << std::setw(2) << std::setfill('0') << static_cast<int>(static_cast<uint8_t>(t));
        }
        std::cout << std::dec << std::endl;

        return true;
    }
    int32_t id;
    if (tileVersion > 7) {
        if (!loadEntityProperties(tile, tileVersion)) {
            return false;
        }

        tile.read(reinterpret_cast<char*>(&id), 4);
        while (id >= 0) {
            int32_t b;
            char c, d;
            tile.read(reinterpret_cast<char*>(&b), 4);
            tile.read(&c, 1);
            tile.read(&d, 1);
            std::cout << "\t\tid: " << id << ' ' << b << ' ' << static_cast<int>(c) << ", dir: " << static_cast<int>(d) << std::endl;
            tile.read(reinterpret_cast<char*>(&id), 4);
        }
        std::cout << "\t\t-1: " << id << std::endl;
    }
    tile.read(reinterpret_cast<char*>(&id), 4);
    while (id >= 0) {
        char l;
        tile.read(&l, 1);
        std::cout << "\t\tid: " << id << ", layer: " << static_cast<int>(l) << std::endl;
        tile.read(reinterpret_cast<char*>(&id), 4);
    }
    std::cout << "\t\t-1: " << id << std::endl;

    int32_t a;
    tile.read(reinterpret_cast<char*>(&a), 4);
    std::cout << "\t\tused kuids count: " << a << std::endl;
    for (int j = 0; j < a; ++j) {
        Kuid kuid(tile);
        int32_t cnt;
        tile.read(reinterpret_cast<char*>(&cnt), 4);
        std::cout << "\t\t\t" << j << ": " << kuid << " - " << cnt << std::endl;
    }
    return true;
}

bool World::loadFiles(const std::filesystem::path &path, const std::string &filename, const std::string &ext) {
    auto indexPath = path / (filename + "_index" + ext);
    if (std::filesystem::exists(indexPath)) {
        std::ifstream index(indexPath, std::ifstream::in | std::ifstream::binary);
        if (!index.is_open()) {
            std::cerr << "file open failed " << indexPath << std::endl;
            return false;
        }
        int32_t indexVersion;
        index.read(reinterpret_cast<char *>(&indexVersion), 4);
        std::cout << "version(?): " << indexVersion << std::endl;
        uint32_t count;
        index.read(reinterpret_cast<char *>(&count), 4);
        std::cout << "tiles count: " << count << std::endl;
        if (!index) {
            std::cerr << "err" << std::endl;
        }
        for (int i = 0; i < count; ++i) {
            sf::Vector2<int16_t> tilePosition;
            index.read(reinterpret_cast<char *>(&tilePosition.x), 2);
            index.read(reinterpret_cast<char *>(&tilePosition.y), 2);
            std::cout << "\ttile #" << i << ' ' << tilePosition;
            if (indexVersion > 1) {
                uint8_t t;
                index.read(reinterpret_cast<char *>(&t), 1);
                std::cout << " ?? (4): " << static_cast<int>(t);
                for (int j = 0; j < t; ++j) {
                    uint8_t tt;
                    index.read(reinterpret_cast<char *>(&tt), 1);
                    std::cout << ' ' << std::hex << static_cast<int>(tt);
                }
            }
            std::cout << std::dec << std::endl;
            auto tileFilename = filename;
            tileFilename += '_' + std::to_string(tilePosition.x) + '_' + std::to_string(tilePosition.y) + ext;
            std::cout << "\tread " << tileFilename << std::endl;
            std::ifstream tile(path / tileFilename, std::ifstream::in | std::ifstream::binary);

            loadFile(tile, 0, tilePosition);
        }
        int32_t n;
        index.read(reinterpret_cast<char *>(&n), 4);
        std::cout << "next available id: " << n << std::endl;
        index.read(reinterpret_cast<char *>(&n), 4);
        std::cout << "used kuids count: " << n << std::endl;
        for (int i = 0; i < n; ++i) {
            Kuid kuid(index);
            int32_t cnt;
            index.read(reinterpret_cast<char *>(&cnt), 4);
            std::cout << "\t" << i << ": " << kuid << " - " << cnt << std::endl;
        }
        return true;
    }
    auto tilePath = path / (filename + ext);
    std::ifstream tile(tilePath);
    if (!tile.is_open()) {
        std::cerr << "file open failed " << tilePath << std::endl;
        return false;
    }
    std::cout << "\tread " << filename << ext << std::endl;
    int32_t a;
    tile.read(reinterpret_cast<char*>(&a), 4);
    std::cout << "\t\ttype: " << a << std::endl;

    loadFile(tile, a);
    if (2 == a) {
        loadFile(tile, a);      //в старых версиях вершины в том же файле, что и секции пути
    }

    return true;
}

bool World::load(const std::filesystem::path& path, const std::string& filename) {
    if (!loadFiles(path, filename, ".trk")) {
        return false;
    }
    if (!loadFiles(path, filename, ".vtx") && false) {
        return false;
    }
    if (!loadFiles(path, filename, ".rlr") && false) {
        return false;
    }
    if (!loadFiles(path, filename, ".obs")) {
        return false;
    }

    return true;
}

bool World::loadEnv(std::istream &in, bool waterColor) {
    int32_t version = 0;
    int32_t cnt;
    in.read(reinterpret_cast<char*>(&cnt), 4);
    if (cnt < 0) {
        std::cout << "-1: " << cnt << ' ';
        in.read(reinterpret_cast<char*>(&version), 4);
        in.read(reinterpret_cast<char*>(&cnt), 4);
    }
    Kuid skyKuid(in);
    std::cout << "envVersion: " << version << ", points count: " << cnt << ", sky: " << skyKuid << std::endl;

    float d, e;
    in.read(reinterpret_cast<char*>(&d), 4);
    in.read(reinterpret_cast<char*>(&e), 4);
    std::cout << "??: " << d << ", curr time(?): " << e << std::endl;
    for (int i = 0; i < cnt; ++i) {
        float t;
        in.read(reinterpret_cast<char*>(&t), 4);
        std::cout << "time: " << t;

        float r, g, b, a;
        in.read(reinterpret_cast<char*>(&r), 4);
        in.read(reinterpret_cast<char*>(&g), 4);
        in.read(reinterpret_cast<char*>(&b), 4);
        in.read(reinterpret_cast<char*>(&a), 4);
        std::cout << "\t\tsun: " << r << ' ' << g << ' ' << b << ' ' << a;
        in.read(reinterpret_cast<char*>(&r), 4);
        in.read(reinterpret_cast<char*>(&g), 4);
        in.read(reinterpret_cast<char*>(&b), 4);
        in.read(reinterpret_cast<char*>(&a), 4);
        std::cout << "\t\tambient: " << r << ' ' << g << ' ' << b << ' ' << a;
        if (version > 3) {
            in.read(reinterpret_cast<char *>(&b), 4);
            std::cout << "\t\tbrightness: " << b;
        }
        in.read(reinterpret_cast<char*>(&r), 4);
        in.read(reinterpret_cast<char*>(&g), 4);
        in.read(reinterpret_cast<char*>(&b), 4);
        in.read(reinterpret_cast<char*>(&a), 4);
        std::cout << "\t\t??: " << r << ' ' << g << ' ' << b << ' ' << a;
        in.read(reinterpret_cast<char*>(&a), 4);
        std::cout << "\t\tfog: " << a;
        if (version > 1) {
            in.read(reinterpret_cast<char *>(&b), 4);
            std::cout << ", ??: " << b;
        }
        in.read(reinterpret_cast<char*>(&r), 4);
        in.read(reinterpret_cast<char*>(&g), 4);
        in.read(reinterpret_cast<char*>(&b), 4);
        in.read(reinterpret_cast<char*>(&a), 4);
        std::cout << "\t\tup: " << r << ' ' << g << ' ' << b << ' ' << a;
        in.read(reinterpret_cast<char*>(&r), 4);
        in.read(reinterpret_cast<char*>(&g), 4);
        in.read(reinterpret_cast<char*>(&b), 4);
        in.read(reinterpret_cast<char*>(&a), 4);
        std::cout << "\t\tmiddle: " << r << ' ' << g << ' ' << b << ' ' << a;
        in.read(reinterpret_cast<char*>(&r), 4);
        in.read(reinterpret_cast<char*>(&g), 4);
        in.read(reinterpret_cast<char*>(&b), 4);
        in.read(reinterpret_cast<char*>(&a), 4);
        std::cout << "\t\tdown: " << r << ' ' << g << ' ' << b << ' ' << a << std::endl;
    }
    float f;
    int32_t aa, bb;
    if (version > 0) {
        in.read(reinterpret_cast<char*>(&aa), 4);
        in.read(reinterpret_cast<char*>(&bb), 4);
        in.read(reinterpret_cast<char*>(&f), 4);
        std::cout << "??: " << std::hex << aa << std::dec << ' ' << bb << ' ' << f << std::endl;
        if (version > 4) {
            char t;
            in.read(&t, 1);
            std::cout << "??: " << static_cast<int>(t) << std::endl;
        }
        in.read(reinterpret_cast<char*>(&f), 4);
        std::cout << "?\?snow(?): " << f << std::endl;
        if (version > 2) {
            std::string h;
            h.resize(4);
            in.read(h.data(), 4);
            std::cout << "soup: " << h << std::endl;
            int32_t k;
            in.read(reinterpret_cast<char *>(&aa), 4);
            in.read(reinterpret_cast<char *>(&bb), 4);
            in.read(reinterpret_cast<char *>(&k), 4);
            std::cout << "??: " << std::hex << aa << std::dec << ' ' << bb << ' ' << k << std::endl;
            if (k) {
                h.resize(k);
                in.read(h.data(), k);
                std::cout << "soup:" << std::hex;
                for (const auto& l : h) {
                    std::cout << ' ' << static_cast<int>(static_cast<unsigned char>(l));
                }
                std::cout << std::dec << std::endl;
            }
        }
    }
    std::cout << "water: ";
    if (waterColor) {
        float _r, _g, _b, _a;
        in.read(reinterpret_cast<char *>(&_r), 4);
        in.read(reinterpret_cast<char *>(&_g), 4);
        in.read(reinterpret_cast<char *>(&_b), 4);
        in.read(reinterpret_cast<char *>(&_a), 4);
        std::cout << _r << ' ' << _g << ' ' << _b << ' ' << _a << ' ';
    }
    Kuid waterKuid(in);
    std::cout << waterKuid << std::endl;


    return true;
}

bool World::loadMap(const std::filesystem::path& path, const std::string& filename) {
    auto gndPath = path / (filename + ".gnd");
    std::ifstream gnd(gndPath, std::ifstream::in | std::ifstream::binary);
    if (!gnd.is_open()) {
        std::cerr << "file open failed " << gndPath << std::endl;
        return false;
    }
    std::string st;
    st.resize(4);
    gnd.read(st.data(), 4);
    uint8_t gndVersion = st[3];
    st.resize(3);
    std::cout << "GND: " << st << ", version(21): " << static_cast<int>(gndVersion) << std::endl;
    if (gndVersion > 14) {
        st.resize(4);
        gnd.read(st.data(), 4);
        std::reverse(st.begin(), st.end());
        auto aa = *reinterpret_cast<int32_t *>(st.data());
        gnd.read(st.data(), 4);
        std::reverse(st.begin(), st.end());
        auto cnt = *reinterpret_cast<int32_t *>(st.data());
        std::cout << "?\?(1): " << aa << ", cnt: " << cnt << std::endl;
        for (int i = 0; i < cnt; ++i) {
            char b, l;
            gnd.read(&b, 1);
            gnd.read(&l, 1);
            st.resize(l);
            gnd.read(st.data(), l);
            std::cout << "??: " << static_cast<int>(b) << ' ' << st;
            gnd.read(&b, 1);
            gnd.read(&l, 1);
            std::cout << ' ' << static_cast<int>(b) << ' ' << static_cast<int>(l);
            st.resize(4);
            for (int j = 0; j < 4; ++j) {
                gnd.read(st.data(), 4);
                std::reverse(st.begin(), st.end());
                auto f = *reinterpret_cast<float *>(st.data());
                std::cout << ' ' << f;
            }
            gnd.read(&b, 1);
            gnd.read(&l, 1);
            std::cout << ' ' << static_cast<int>(b) << ' ' << static_cast<int>(l) << std::endl;
        }
    }
    int32_t cnt;
    gnd.read(reinterpret_cast<char*>(&cnt), 4);
    std::cout << "tiles count: " << cnt << std::endl;
    for (int i = 0; i < cnt; ++i) {
        sf::Vector2<int32_t> tilePosition;
        gnd.read(reinterpret_cast<char*>(&tilePosition.x), 4);
        gnd.read(reinterpret_cast<char*>(&tilePosition.y), 4);
        EntityType type = "Secs";
        if (gndVersion > 11) {
            uint32_t t;
            gnd.read(reinterpret_cast<char*>(&t), 4);
            type = t;
        }
        std::cout << "\ttile #" << i << ": " << tilePosition << ", type: " << type;
        std::ifstream tile;
        int32_t tileVersion;
        if (gndVersion > 14) {
            if (gndVersion > 20) {
                char c;
                gnd.read(&c, 1);
                std::cout << " ?? (4): " << static_cast<int>(c) << std::hex;
                for (int j = 0; j < c; ++j) {
                    uint8_t tt;
                    gnd.read(reinterpret_cast<char *>(&tt), 1);
                    std::cout << ' ' << static_cast<int>(tt);
                }
            }
            int32_t d;
            gnd.read(reinterpret_cast<char *>(&d), 4);
            std::cout << std::dec << ", used kuids: " << d << std::endl;
            for (int j = 0; j < d; ++j) {
                Kuid kuid(gnd);
                std::cout << "\t\t" << kuid << std::endl;
            }
continue;
            auto tileFilename = filename;
            tileFilename += '_' + std::to_string(tilePosition.x) + '_' + std::to_string(tilePosition.y) + ".gnd";
            std::cout << "\tread " << tileFilename << std::endl;
            tile.open(path / tileFilename, std::ifstream::in | std::ifstream::binary);
            tile.read(reinterpret_cast<char *>(&tileVersion), 4);
            std::cout << "\t\tversion (8): " << tileVersion << std::endl;
        }
        else {
            int32_t offset1, offset2;
            gnd.read(reinterpret_cast<char*>(&offset1), 4);
            gnd.read(reinterpret_cast<char*>(&offset2), 4);
            std::cout << "\toffset1: " << offset1 << ", offset2: " << offset2 << std::endl;
continue;
            tile.open(gndPath);
            tile.seekg(offset1);
            tileVersion = gndVersion;
        }
        int32_t n;
        if (tileVersion > 11) {
            tile.read(reinterpret_cast<char *>(&n), 4);
            std::cout << "\t\tpaint slots count: " << n << std::endl;
            for (int j = 0; j < n; ++j) {
                float c;
                tile.read(reinterpret_cast<char *>(&c), 4);
                if (c > 0) {
                    Kuid kuid(tile);
                    std::cout << "\t\t\t" << j << ": " << kuid << " - " << c << std::endl;
                }
            }
        }
        tile.read(reinterpret_cast<char*>(&n), 4);
        std::cout << "\t\twater count: " << n << std::endl;
        for (int j = 0; j < n; ++j) {
            sf::Vector2<int16_t> coord;
            float h;
            tile.read(reinterpret_cast<char*>(&coord.x), 2);
            tile.read(reinterpret_cast<char*>(&coord.y), 2);
            tile.read(reinterpret_cast<char*>(&h), 4);
            std::cout << "\t\t\t" << coord << " - " << h << std::endl;
        }
        int tileSize = 76;
        if (type == "Sec5") {
            tileSize = 148;
        }
        for (int j = 0; j < tileSize; ++j) {
            for (int k = 0; k < tileSize; ++k) {
                char paints;
                tile.read(&paints, 1);
                if (-3 == paints) {
                    char paint, dir;
                    tile.read(&paint, 1);
                    tile.read(&dir, 1);
                    float h;
                    tile.read(reinterpret_cast<char*>(&h), 4);
//                    std::cout << "\t\t" << sf::Vector2{j, k} << " paints: " << static_cast<int>(paints) << ", p: " << static_cast<int>(paint) << ", d: " << static_cast<int>(dir) << ", h: " << h << std::endl;
                    if (tileVersion < 12) {
                        char c1, c2, c3;
                        tile.read(&c1, 1);
                        tile.read(&c2, 1);
                        tile.read(&c3, 1);
//                        std::cout << "\t\t\t??: " << static_cast<int>(c1) << ' ' << static_cast<int>(c2) << ' ' << static_cast<int>(c3) << std::endl;
                    }
                }
                else if (-2 == paints) {
                    char paint, dir;
                    tile.read(&paint, 1);
                    tile.read(&dir, 1);
//                    std::cout << "\t\t" << sf::Vector2{j, k} << " paints: " << static_cast<int>(paints) << ", p: " << static_cast<int>(paint) << ", d: " << static_cast<int>(dir) << std::endl;
                }
                else if (paints >= 0) {
                    std::cout << "\t\t" << sf::Vector2{j, k} << ' ';
                    while (paints >= 0) {
                        char w, dir;
                        tile.read(&w, 1);
                        tile.read(&dir, 1);
                        std::cout << "paint: " << static_cast<int>(static_cast<uint8_t>(paints)) << ", w: " << static_cast<int>(static_cast<uint8_t>(w)) << ", dir: " << static_cast<int>(dir) << "; ";
                        tile.read(&paints, 1);
                    }
                    float h;
                    tile.read(reinterpret_cast<char*>(&h), 4);
                    std::cout << static_cast<int>(paints) << ", h: " << h << std::endl;
                    if (tileVersion < 12) {
                        char c1, c2, c3;
                        tile.read(&c1, 1);
                        tile.read(&c2, 1);
                        tile.read(&c3, 1);
                        std::cout << "\t\t\t??: " << static_cast<int>(c1) << ' ' << static_cast<int>(c2) << ' ' << static_cast<int>(c3) << std::endl;
                    }
                }
                else {
                    std::cout << "unknown paints count: " << static_cast<int>(paints) << ", dump:" << std::hex << std::setfill('0');
                    for (int l = 0; l < 16; ++l) {
                        char tt;
                        tile.read(&tt, 1);
                        std::cout << ' ' << std::setw(2) << static_cast<int>(static_cast<uint8_t>(tt));
                    }
                    std::cout << std::dec << std::endl;
                    return true;
                }
            }
        }
        if (tileVersion > 14) {
            int16_t c;
            tile.read(reinterpret_cast<char*>(&c), 2);
            std::cout << "\t\t??: " << c;
            for (int j = 0; j < c; ++j) {
                float f;
                tile.read(reinterpret_cast<char*>(&f), 4);
                std::cout << ' ' << f;
            }
            std::cout << std::endl;
        }
    }

    loadEnv(gnd, gndVersion > 10);

    if (gndVersion < 14) {
        loadFile(gnd, 13);
    }
    if (gndVersion < 12) {
        gnd.read(reinterpret_cast<char*>(&cnt), 4);
        std::cout << "\t\tpaint slots count: " << cnt << std::endl;
        for (int j = 0; j < cnt; ++j) {
            float c;
            gnd.read(reinterpret_cast<char*>(&c), 4);
//            if (c > 0) {
                Kuid kuid(gnd);
                std::cout << "\t\t\t" << j << ": " << kuid << " - " << c << std::endl;
//            }
        }
    }

    if (!load(path, filename)) {
        return false;
    }

    return true;
}

bool World::loadProfile(const std::filesystem::path& path, const std::string& filename) {
    if (!load(path, filename)) {
        return false;
    }

    auto datPath = path / (filename + ".dat");
    std::ifstream dat(datPath, std::ifstream::in | std::ifstream::binary);
    if (!dat.is_open()) {
        std::cerr << "file open failed " << datPath << std::endl;
        return false;
    }
    int32_t datVersion, len2;
    dat.read(reinterpret_cast<char*>(&datVersion), 4);
    dat.read(reinterpret_cast<char*>(&len2), 4);
    std::cout << "version(4): " << datVersion << ", len: " << len2 << std::endl;
    if (len2) {
        std::string str2;
        str2.resize(len2);
        dat.read(str2.data(), len2);
        std::istringstream buf2(str2);

        int32_t version, a;
        buf2.read(reinterpret_cast<char*>(&version), 4);
        buf2.read(reinterpret_cast<char*>(&a), 4);
        std::cout << "version(8): " << version << ", ??: " << a << std::endl;
        loadEntities(buf2);

        int32_t b, c, d, e, f;
        buf2.read(reinterpret_cast<char*>(&b), 4);
        buf2.read(reinterpret_cast<char*>(&c), 4);
        buf2.read(reinterpret_cast<char*>(&d), 4);
        buf2.read(reinterpret_cast<char*>(&e), 4);
        buf2.read(reinterpret_cast<char*>(&f), 4);
        std::cout << "??: " << b << ' ' << std::hex << c << std::dec << ' ' << d << ' ' << e << ", -1: " << f << std::endl;

        std::cout << "data:";
        char tt = 0;
        buf2.read(&tt, 1);
        while (!buf2.eof()) {
            std::cout << ' ' << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(static_cast<uint8_t>(tt));
            buf2.read(&tt, 1);
        }
        std::cout << std::dec << std::endl << std::endl;
    }

    if (datVersion > 1) {
        dat.read(reinterpret_cast<char*>(&len2), 4);
        std::cout << "len: " << len2 << std::endl;
        if (len2) {
            std::string str2;
            str2.resize(len2);
            dat.read(str2.data(), len2);
            std::istringstream buf2(str2);

            int32_t a, b, version, id;
            buf2.read(reinterpret_cast<char*>(&a), 4);
            buf2.read(reinterpret_cast<char*>(&b), 4);
            buf2.read(reinterpret_cast<char*>(&version), 4);
            buf2.read(reinterpret_cast<char*>(&id), 4);
            std::cout << "??: " << a << ' ' << std::hex << b << std::dec << ", version: " << version << std::endl;
            while (id >= 0) {
                int32_t len;
                buf2.read(reinterpret_cast<char*>(&len), 4);
                std::cout << "id: " << id << ", len: " << len << ", data:" << std::hex;
                for (int i = 0; i < len; ++i) {
                    char c;
                    buf2.read(&c, 1);
                    std::cout << ' ' << static_cast<int>(static_cast<uint8_t>(c));
                }
                std::cout << std::dec << std::endl;

                buf2.read(reinterpret_cast<char*>(&id), 4);
            }
            std::cout << "-1: " << id << std::endl;

            std::cout << "data:";
            char tt = 0;
            buf2.read(&tt, 1);
            while (!buf2.eof()) {
                std::cout << ' ' << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(static_cast<uint8_t>(tt));
                buf2.read(&tt, 1);
            }
            std::cout << std::dec << std::endl << std::endl;
        }
    }

    dat.read(reinterpret_cast<char*>(&len2), 4);
    std::cout << "len: " << len2 << std::endl;
    if (len2) {
        std::string str2;
        str2.resize(len2);
        dat.read(str2.data(), len2);
        std::istringstream buf2(str2);

        int32_t version, a;
        buf2.read(reinterpret_cast<char*>(&version), 4);
        buf2.read(reinterpret_cast<char*>(&a), 4);
        std::cout << "version: " << version << ' ' << a << std::endl;

        int32_t id;
        buf2.read(reinterpret_cast<char*>(&id), 4);
        while (id >= 0) {
            uint32_t t;
            buf2.read(reinterpret_cast<char*>(&t), 4);
            Kuid kuid(buf2);
            uint32_t len;
            buf2.read(reinterpret_cast<char*>(&len), 4);
            auto type = EntityType(t);
            std::cout << "entity type: " << type << ", id: " << id << ", kuid: " << kuid << ", len: " << len << std::endl;
            std::string str;
            str.resize(len);
            buf2.read(str.data(), len);
            std::istringstream buf(str);

            if (TrainConsist::_type == type) {
                auto train = TrainConsist(id, kuid);
//                train.loadBody(buf);
            }
            else {
                std::cerr << "unknown type " << type << std::endl;
            }

            std::cout << "\tdata:";
            char tt = 0;
            buf.read(&tt, 1);
            while (!buf.eof()) {
                std::cout << ' ' << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(static_cast<uint8_t>(tt));
                buf.read(&tt, 1);
            }
            std::cout << std::dec << std::endl;

            buf2.read(reinterpret_cast<char*>(&id), 4);
        }
        std::cout << "-1: " << id << std::endl;

        std::cout << "data:";
        char tt = 0;
        buf2.read(&tt, 1);
        while (!buf2.eof()) {
            std::cout << ' ' << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(static_cast<uint8_t>(tt));
            buf2.read(&tt, 1);
        }
        std::cout << std::dec << std::endl << std::endl;
    }

    dat.read(reinterpret_cast<char*>(&len2), 4);
    std::cout << "len: " << len2 << std::endl;
    if (len2) {
        std::string str2;
        str2.resize(len2);
        dat.read(str2.data(), len2);
        std::istringstream buf2(str2);

        int32_t a, b, version, tileType, id;
        buf2.read(reinterpret_cast<char*>(&a), 4);
        buf2.read(reinterpret_cast<char*>(&b), 4);
        buf2.read(reinterpret_cast<char*>(&version), 4);
        buf2.read(reinterpret_cast<char*>(&tileType), 4);

        std::cout << "??: " << a << ' ' << std::hex << b << std::dec << ", version: " << version << ", type: " << tileType << std::endl;
        if (!loadEntityProperties(buf2, version)) {
            return false;
        }

        std::cout << "data:";
        char tt = 0;
        buf2.read(&tt, 1);
        while (!buf2.eof()) {
            std::cout << ' ' << ' ' << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(static_cast<uint8_t>(tt));
            buf2.read(&tt, 1);
        }
        std::cout << std::dec << std::endl << std::endl;
    }

    dat.read(reinterpret_cast<char*>(&len2), 4);
    std::cout << "len: " << len2 << std::endl;
    if (len2) {
        std::string str2;
        str2.resize(len2);
        dat.read(str2.data(), len2);
        std::istringstream buf2(str2);

        int32_t version, a;
        buf2.read(reinterpret_cast<char*>(&version), 4);
        buf2.read(reinterpret_cast<char*>(&a), 4);
        std::cout << "version(8): " << version << ", ??: " << a << std::endl;
        loadEntities(buf2);

        int32_t b, c, d, e, f;
        buf2.read(reinterpret_cast<char*>(&b), 4);
        buf2.read(reinterpret_cast<char*>(&c), 4);
        buf2.read(reinterpret_cast<char*>(&d), 4);
        buf2.read(reinterpret_cast<char*>(&e), 4);
        buf2.read(reinterpret_cast<char*>(&f), 4);
        std::cout << "??: " << b << ' ' << std::hex << c << std::dec << ' ' << d << ' ' << e << ", -1: " << f << std::endl;

        std::cout << "data:";
        char tt = 0;
        buf2.read(&tt, 1);
        while (!buf2.eof()) {
            std::cout << ' ' << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(static_cast<uint8_t>(tt));
            buf2.read(&tt, 1);
        }
        std::cout << std::dec << std::endl << std::endl;
    }

    if (datVersion > 1) {
        dat.read(reinterpret_cast<char*>(&len2), 4);
        std::cout << "len: " << len2 << std::endl;
        if (len2) {
            std::string str2;
            str2.resize(len2);
            dat.read(str2.data(), len2);
            std::istringstream buf2(str2);

            int32_t version;
            buf2.read(reinterpret_cast<char*>(&version), 4);
            std::cout << "version(3): " << version << std::endl;
            float a0;
            int32_t aa, c;
            buf2.read(reinterpret_cast<char*>(&aa), 4);
            buf2.read(reinterpret_cast<char*>(&c), 4);
            std::cout << "date: " << aa << ' ' << c << std::endl;
            float d, e;
            buf2.read(reinterpret_cast<char*>(&d), 4);
            buf2.read(reinterpret_cast<char*>(&e), 4);
            std::cout << "??: " << d << ", snow: " << e << std::endl;
            if (version > 0) {
                loadEnv(buf2);
            }
            std::cout << "data:";
            char tt = 0;
            buf2.read(&tt, 1);
            while (!buf2.eof()) {
                std::cout << ' ' << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(static_cast<uint8_t>(tt));
                buf2.read(&tt, 1);
            }
            std::cout << std::dec << std::endl << std::endl;
        }

        int32_t b, c;
        dat.read(reinterpret_cast<char*>(&b), 4);
        dat.read(reinterpret_cast<char*>(&c), 4);
        std::cout << "??: " << b << ' ' << c << std::endl;
        if (b) {
            return true;
        }

        dat.read(reinterpret_cast<char*>(&len2), 4);
        std::cout << "len: " << len2 << std::endl;
        if (len2) {
            std::string str2;
            str2.resize(len2);
            dat.read(str2.data(), len2);
            std::istringstream buf2(str2);

            int32_t version, d;
            buf2.read(reinterpret_cast<char*>(&version), 4);
            std::cout << "version(25): " << version << std::endl;

            std::cout << "data:";
            char tt = 0;
            buf2.read(&tt, 1);
            while (!buf2.eof()) {
                std::cout << ' ' << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(static_cast<uint8_t>(tt));
                buf2.read(&tt, 1);
            }
            std::cout << std::dec << std::endl << std::endl;
        }
    }

    return true;
}

bool World::save(const std::filesystem::path& path) const {
    return false;
}

void World::close() {
    tiles.clear();
    allVtx.clear();
    allTrk.clear();
    allRules.clear();
    allObjects.clear();
}

World::~World() {
    close();
}

void World::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    for (auto& [coord, tile] : tiles) {
        if (coord.x >= viewAreaMin.x && coord.x <= viewAreaMax.x && coord.y >= viewAreaMin.y && coord.y <= viewAreaMax.y) {
            tile.draw(target, states);
        }
    }
}

void World::render(wxDC& dc, const CoordinateTransformer& transformer) {
    for (auto& [coord, tile] : tiles) {
        if (transformer.isTileInViewport({coord.x, coord.y})) {
            tile.render(dc, transformer);
        }
    }
}

