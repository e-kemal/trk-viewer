#include <iostream>

#ifndef TRK_VIEWER_KUID_H
#define TRK_VIEWER_KUID_H

class Kuid {
public:
    int _author_id;
    int _content_id;
    int _version;
    Kuid() = default;
    explicit Kuid(std::istream& istream);
    void load(std::istream& istream);
    void save(std::ostream& ostream) const;
};

std::ostream& operator<< (std::ostream& ostream, const Kuid& kuid);

#endif //TRK_VIEWER_KUID_H
