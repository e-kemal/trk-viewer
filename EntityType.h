#ifndef TRK_VIEWER_ENTITYTYPE_H
#define TRK_VIEWER_ENTITYTYPE_H


#include <cstdint>
#include <ostream>

class EntityType {

    uint32_t _type{};
    friend std::ostream& operator<< (std::ostream&, const EntityType&);
public:
    constexpr EntityType() = default;
    constexpr EntityType(uint32_t t) : _type(t) {}
//    EntityType(const std::string& s);
    [[maybe_unused]] constexpr EntityType(const char s[4]) {
        for (int i = 0; i < 4; ++i) {
            _type <<= 8;
            _type += static_cast<unsigned char>(s[i]);
        }
    }
    bool operator==(const EntityType& other) const {
        return other._type == _type;
    }
};

std::ostream& operator<< (std::ostream& ostream, const EntityType& type);

#endif //TRK_VIEWER_ENTITYTYPE_H
