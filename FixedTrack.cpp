#include "FixedTrack.h"

void FixedTrack::loadBody(std::istream &istream) {
    Scenery::loadBody(istream);

    int32_t a, count;
    istream.read(reinterpret_cast<char*>(&a), 4);
    if (istream.eof()) {
        return;
    }
    istream.read(reinterpret_cast<char*>(&count), 4);
std::cout << "\t\t\t??: " << a << ", Count: " << count << ':';
    for (int i = 0; i < count; ++i) {
        int32_t x;
        istream.read(reinterpret_cast<char*>(&x), 4);
std::cout << ' ' << x;
    }
std::cout << std::endl;

    istream.read(reinterpret_cast<char*>(&count), 4);
std::cout << "\t\t\t??: Count: " << count << ':';
    for (int i = 0; i < count; ++i) {
        int32_t x;
        istream.read(reinterpret_cast<char*>(&x), 4);
std::cout << ' ' << x;
    }
std::cout << std::endl;

    istream.read(reinterpret_cast<char*>(&count), 4);
std::cout << "\t\t\t??: Count: " << count << ':';
    for (int i = 0; i < count; ++i) {
        int32_t x, y;
        istream.read(reinterpret_cast<char*>(&x), 4);
        istream.read(reinterpret_cast<char*>(&y), 4);
std::cout << ' ' << x << '_' << y;
    }
std::cout << std::endl;
}

void Turntable::loadBody(std::istream &istream) {
    FixedTrack::loadBody(istream);
    float a;
    int32_t b, c;
    istream.read(reinterpret_cast<char*>(&a), 4);
    istream.read(reinterpret_cast<char*>(&b), 4);
    istream.read(reinterpret_cast<char*>(&c), 4);
std::cout << "\t\t\t??: " << a << ' ' << b << ' ' << c << std::endl;
}

void Buildable::loadBody(std::istream &istream) {
    FixedTrack::loadBody(istream);

    if (istream.eof()) {
        return;
    }
    int32_t a;
    float b;
    istream.read(reinterpret_cast<char*>(&a), 4);
    istream.read(reinterpret_cast<char*>(&b), 4);
std::cout << "\t\t\t??: " << a << ' ' << b << std::endl;
}
