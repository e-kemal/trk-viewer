#ifndef TRK_VIEWER_BEHAVIOR_H
#define TRK_VIEWER_BEHAVIOR_H


#include "Entity.h"

class Behavior : public Entity {
public:
    std::string name;
    Behavior(const int32_t& id, const Kuid& kuid) : Entity(id, kuid) {}
    static constexpr EntityType _type = "BHVR";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
    void saveBody(std::ostream &ostream) const override;
};

class Library : public Entity {
public:
    Library(const int32_t& id, const Kuid& kuid) : Entity(id, kuid) {}
    static constexpr EntityType _type = "gslb";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
    void saveBody(std::ostream &ostream) const override;
};

class TrainConsist : public Entity {
public:
    TrainConsist(const int32_t& id, const Kuid& kuid) : Entity(id, kuid) {}
    static constexpr EntityType _type = "trCt";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
    void saveBody(std::ostream &ostream) const override;
};


#endif //TRK_VIEWER_BEHAVIOR_H
