#include <iomanip>
#include "BridgeSection.h"
#include "vectorsHelper.h"

void BridgeSection::loadBody(std::istream &istream) {
    int32_t brVersion;
    istream.read(reinterpret_cast<char*>(&brVersion), 4);
std::cout << "\t\t\tbrVersion (3): " << brVersion << std::endl;

    TrackSection::loadBody(istream);

    if (brVersion > 2) {
        int8_t cnt;
        istream.read(reinterpret_cast<char *>(&cnt), 1);
        for (int i = 0; i < cnt; ++i) {
            int32_t a, b, c;
            istream.read(reinterpret_cast<char *>(&a), 4);
            istream.read(reinterpret_cast<char *>(&b), 4);
            istream.read(reinterpret_cast<char *>(&c), 4);
            std::cout << "\t\t\tatt track " << i << ": vertex: " << a << ", vertex: " << b << ", track: " << c << std::endl;
        }
    }
    else {
        std::cout << "\t\t\t??:";
        for (int i = 0; i < 15; ++i) {
            int32_t a;
            istream.read(reinterpret_cast<char *>(&a), 4);
            std::cout << ' ' << a;
        }
        std::cout << std::endl;
    }
}

void BridgeSection::saveBody(std::ostream &ostream) const {

}
