#ifndef TRK_VIEWER_TRKVIEWERPANEL_H
#define TRK_VIEWER_TRKVIEWERPANEL_H


#include <wx/panel.h>
#include "World.h"

class TrkViewerPanel : public wxPanel {
    World& world;
    double zoom = 1.f;
    wxPoint2DInt viewTile{};
    wxPoint2DDouble viewOffset{};
    wxPoint2DInt moveBaseTile{};
    wxPoint2DDouble moveBaseOffset{};
    bool moving = false;
    wxPoint selectStart{}, selectEnd{};
    bool selecting = false;
public:
    explicit TrkViewerPanel(wxWindow* parent, World& world) : wxPanel(parent), world(world) {}

    void eraseBackground(wxEraseEvent& event);
    void paintEvent(wxPaintEvent& event);
    void paintNow();

    void render(wxDC& dc);

    void mouseLeftDown(wxMouseEvent& event);
    void mouseLeftUp(wxMouseEvent& event);
    void mouseRightDown(wxMouseEvent& event);
    void mouseRightUp(wxMouseEvent& event);
    void mouseMoved(wxMouseEvent& event);
    void mouseWheelMoved(wxMouseEvent& event);
    void mouseLeftWindow(wxMouseEvent& event);
    void keyPressed(wxKeyEvent& event);
    void keyReleased(wxKeyEvent& event);
    void resize(wxSizeEvent& event);

    DECLARE_EVENT_TABLE()
};


#endif //TRK_VIEWER_TRKVIEWERPANEL_H
