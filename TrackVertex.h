#include "Entity.h"
#include "WorldCoordinate.h"
#include "BoundaryBox.h"

#ifndef TRK_VIEWER_TRACKVERTEX_H
#define TRK_VIEWER_TRACKVERTEX_H

class TrackVertex : public Entity {
public:
    WorldCoordinate coordinate;
    int32_t track_id[4];
    int8_t dir{};
    float superElevationDegree{}, superElevationLimit{};
    int32_t scenery_id1 = -1;
    int32_t scenery_id2 = -1;
    uint32_t flags{};
    TrackVertex(const int32_t& id, const Kuid& kuid) : Entity(id, kuid), coordinate(), track_id() {};
    static constexpr EntityType _type = "tkVx";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
    void saveBody(std::ostream &ostream) const override;
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
    void render(wxDC& dc, const CoordinateTransformer& transformer) const override;
};


#endif //TRK_VIEWER_TRACKVERTEX_H
