#include <wx/sizer.h>
#include <wx/glcanvas.h>
#include <wx/menu.h>
#include <wx/msgdlg.h>
#include <wx/log.h>
#include <wx/dcclient.h>
#include "TrkViewerFrame.h"
#include "TrkViewerPanel.h"

const int TrkViewerFrame::idHello = wxNewId();

wxBEGIN_EVENT_TABLE(TrkViewerFrame, wxFrame)
                EVT_MENU(idHello, TrkViewerFrame::OnHello)
                EVT_MENU(wxID_EXIT, TrkViewerFrame::OnExit)
                EVT_MENU(wxID_ABOUT, TrkViewerFrame::OnAbout)
wxEND_EVENT_TABLE()

TrkViewerFrame::TrkViewerFrame(const wxString& title, const wxPoint& pos, const wxSize& size, World& world)
    : wxFrame(nullptr, wxID_ANY, title, pos, size), world(world)
{
    auto sizer = new wxBoxSizer(wxHORIZONTAL);
    drawPanel = new TrkViewerPanel(this, world);

    sizer->Add(drawPanel, 1, wxEXPAND);

    SetSizer(sizer);
    SetAutoLayout(true);

    auto menuFile = new wxMenu;
    menuFile->Append(idHello, "&Hello...\tCtrl-H", "Help string shown in status bar for this menu item");
    menuFile->AppendSeparator();
    menuFile->Append(wxID_EXIT);

    auto menuHelp = new wxMenu;
    menuHelp->Append(wxID_ABOUT);

    auto menuBar = new wxMenuBar;
    menuBar->Append(menuFile, "&File");
    menuBar->Append(menuHelp, "&Help");

    SetMenuBar(menuBar);

    CreateStatusBar();
    SetStatusText("Welcome to wxWidgets!");
}

void TrkViewerFrame::OnHello(wxCommandEvent &event) {
    wxLogMessage("Hello world from wxWidgets!");
}

void TrkViewerFrame::OnExit(wxCommandEvent& event) {
    Close(true);
}

void TrkViewerFrame::OnAbout(wxCommandEvent& event) {
    wxMessageBox("This is a wxWidgets' Hello world sample", "About Hello World", wxOK | wxICON_INFORMATION);
}

