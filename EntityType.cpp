#include "EntityType.h"


std::ostream& operator<<(std::ostream& out, const EntityType& type) {
    auto tmp = type._type;
    char buf[5];
    buf[4] = 0;
    for (int i = 3; i >= 0; --i) {
        buf[i] = static_cast<char>(tmp & 0xff);
        tmp >>= 8;
    }
    out << std::string(buf);

    return out;
}
