#ifndef TRK_VIEWER_TRKVIEWERFRAME_H
#define TRK_VIEWER_TRKVIEWERFRAME_H


#include <wx/frame.h>
#include "TrkViewerPanel.h"
#include "World.h"

class TrkViewerFrame : public wxFrame {
public:
    TrkViewerFrame(const wxString& title, const wxPoint& pos, const wxSize& size, World& world);
private:
    World& world;
    TrkViewerPanel* drawPanel;
    void OnHello(wxCommandEvent& event);
    void OnExit(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);

    static const int idHello;

    wxDECLARE_EVENT_TABLE();
};


#endif //TRK_VIEWER_TRKVIEWERFRAME_H
