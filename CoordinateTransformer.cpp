#include "CoordinateTransformer.h"

bool CoordinateTransformer::isTileInViewport(const wxPoint2DInt &tile) const {
    return tile.m_y >= _tile.m_x - 1 && tile.m_y <= _tile.m_x + _sizeTiles.m_x && tile.m_x >= _tile.m_y - 1 && tile.m_x <= _tile.m_y + _sizeTiles.m_y;
}

wxPoint CoordinateTransformer::operator()(const WorldCoordinate &coordinate) const {
    return {
        static_cast<int>(((coordinate._baseboard.y - _tile.m_x) * 720. + coordinate._offset.y - _offset.m_x) * _zoom),
        static_cast<int>(((coordinate._baseboard.x - _tile.m_y) * 720. + coordinate._offset.x - _offset.m_y) * _zoom),
    };
}

wxPoint CoordinateTransformer::operator()(const wxPoint2DInt &tile, const wxPoint2DDouble &offset) const {
    return {
        static_cast<int>(((tile.m_y - _tile.m_x) * 720. + offset.m_y - _offset.m_x) * _zoom),
        static_cast<int>(((tile.m_x - _tile.m_y) * 720. + offset.m_x - _offset.m_y) * _zoom),
    };
}

wxPoint CoordinateTransformer::operator()(const sf::Vector2<int16_t> &tile, const sf::Vector3f &offset) const {
    return {
            static_cast<int>(((tile.y - _tile.m_x) * 720. + offset.y - _offset.m_x) * _zoom),
            static_cast<int>(((tile.x - _tile.m_y) * 720. + offset.x - _offset.m_y) * _zoom),
    };
}
