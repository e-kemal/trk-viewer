#include <SFML/Graphics/Vertex.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include "Rule.h"
#include "vectorsHelper.h"

void Rule::loadBody(std::istream &istream) {
    coordinate1.load(istream);
    coordinate2.load(istream);
std::cout << "\t\t\tcoord1: " << coordinate1 << std::endl;
std::cout << "\t\t\tcoord2: " << coordinate2 << std::endl;
    char t;
    istream.read(&t, 1);
std::cout << "\t\t\tlayer: " << static_cast<int>(t) << std::endl;
}

void Rule::saveBody(std::ostream &ostream) const {

}

void Rule::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    sf::Vertex line[] = {
            sf::Vertex(to2d(coordinate1), sf::Color::Yellow),
            sf::Vertex(to2d(coordinate2), sf::Color::Yellow),
    };
    target.draw(line, 2, sf::Lines);

    sf::CircleShape circle{.5f};
    circle.setFillColor(sf::Color::Yellow);
    circle.setPosition(line[0].position - sf::Vector2f{.5f, .5f});
    target.draw(circle);

    circle.setPosition(line[1].position - sf::Vector2f{.5f, .5f});
    target.draw(circle);
}

void Rule::render(wxDC& dc, const CoordinateTransformer& transformer) const {
    dc.SetPen(*wxYELLOW_PEN);
    dc.SetBrush(*wxYELLOW_BRUSH);
    auto point1 = transformer(coordinate1);
    auto point2 = transformer(coordinate2);
    dc.DrawLine(point1, point2);
    dc.DrawCircle(point1, 3);
    dc.DrawCircle(point2, 3);
}
