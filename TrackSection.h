#include "Entity.h"
#include <SFML/System/Vector3.hpp>
#include "BoundaryBox.h"

#ifndef TRK_VIEWER_TRACKSECTION_H
#define TRK_VIEWER_TRACKSECTION_H


class TrackSection : public Entity {
public:
    int32_t vertex1_id{};
    int32_t vertex2_id{};
    Kuid kuid1{};
    uint32_t flags{};
    int8_t layer_id{};
    std::string name{};
    float quality{};
    float length{}, length2{};
    sf::Vector3f tangent1{}, tangent2{};
    sf::Vector3f data1, data2, delta, vertex1;
    BoundaryBox box{};
    sf::Vector2<int16_t> inTile;
    int32_t scenery_id{};
    std::string in_scenery_name{};
    int32_t inBridge{};
    TrackSection(const int32_t& id, const Kuid& kuid) : Entity(id, kuid) {};
    static constexpr EntityType _type = "tkSt";
    [[nodiscard]] EntityType getType() const override {
        return _type;
    }
    void loadBody(std::istream &istream) override;
    void saveBody(std::ostream &ostream) const override;
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
    void render(wxDC& dc, const CoordinateTransformer& transformer) const override;
};


#endif //TRK_VIEWER_TRACKSECTION_H
