#include "TrkViewerApp.h"
#include "TrkViewerFrame.h"

wxIMPLEMENT_APP(TrkViewerApp);

bool TrkViewerApp::OnInit() {
//    wxInitAllImageHandlers();
    auto frame = new TrkViewerFrame("Trk Viewer", wxPoint(50, 50), wxSize(800, 800), world);
    frame->Show(true);
//    SetTopWindow(frame);
    if (argc > 1) {
        world.loadMap(argv[1].ToStdString(), "mapfile");
    }
    if (argc > 2) {
        world.loadProfile(argv[2].ToStdString(), "profile");
    }

    return true;
}
