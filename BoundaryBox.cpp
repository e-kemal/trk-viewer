#include "BoundaryBox.h"
#include "vectorsHelper.h"

BoundaryBox::BoundaryBox(std::istream &istream) : BoundaryBox() {
    load(istream);
}

void BoundaryBox::load(std::istream &istream) {
    istream.read(reinterpret_cast<char*>(&_baseboard.x), 2);
    istream.read(reinterpret_cast<char*>(&_baseboard.y), 2);
    istream.read(reinterpret_cast<char*>(&_x_min), 4);
    istream.read(reinterpret_cast<char*>(&_x_max), 4);
    istream.read(reinterpret_cast<char*>(&_y_min), 4);
    istream.read(reinterpret_cast<char*>(&_y_max), 4);
    istream.read(reinterpret_cast<char*>(&_z_min), 4);
    istream.read(reinterpret_cast<char*>(&_z_max), 4);
}

void BoundaryBox::save(std::ostream &ostream) const {
    ostream.write(reinterpret_cast<const char*>(&_baseboard.x), 2);
    ostream.write(reinterpret_cast<const char*>(&_baseboard.y), 2);
    ostream.write(reinterpret_cast<const char*>(&_x_min), 4);
    ostream.write(reinterpret_cast<const char*>(&_x_max), 4);
    ostream.write(reinterpret_cast<const char*>(&_y_min), 4);
    ostream.write(reinterpret_cast<const char*>(&_y_max), 4);
    ostream.write(reinterpret_cast<const char*>(&_z_min), 4);
    ostream.write(reinterpret_cast<const char*>(&_z_max), 4);
}

std::ostream& operator<< (std::ostream& ostream, const BoundaryBox& box)
{
    ostream << "tile: " << box._baseboard << ", min: (" << box._x_min << ", " << box._y_min << ", " << box._z_min << "), max: (" << box._x_max << ", " << box._y_max << ", " << box._z_max << ')';
    return ostream;
}
