#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include "Scenery.h"
#include "vectorsHelper.h"

void SceneryAbstract::loadBody(std::istream &istream) {

    char version;
    istream.read(&version, 1);
    std::cout << "\t\t\tversion(1..3): " << static_cast<int>(version) << std::endl;
    coordinate.load(istream);
    std::cout << "\t\t\tcoordinate: " << coordinate << std::endl;
    istream.read(reinterpret_cast<char*>(&rotate), 4);
    istream.read(reinterpret_cast<char*>(&incline), 4);
    if (version > 2) {
        istream.read(reinterpret_cast<char *>(&f), 4);
    }
    istream.read(reinterpret_cast<char*>(&height), 4);
    std::cout << "\t\t\trotate: " << rotate << ", incline: " << incline << ", ??: " << f << ", height: " << height << std::endl;
    if (version > 0) {
        istream.read(reinterpret_cast<char *>(&layer), 1);
    }
    char c = 0;
    if (version > 1) {
        istream.read(&c, 1);
    }
    std::cout << "\t\t\tlayer: " << static_cast<int>(layer) << ", ??: " << static_cast<int>(c) << std::endl;
}

void SceneryAbstract::saveBody(std::ostream &ostream) const {

}

void SceneryAbstract::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    sf::RectangleShape rectangle({1.f, 1.f});
    rectangle.setFillColor(sf::Color::Transparent);
    rectangle.setOutlineColor(sf::Color::Green);
    rectangle.setOutlineThickness(.2f);
    rectangle.setPosition(to2d(coordinate) - sf::Vector2f(.5f, .5f));
    target.draw(rectangle);
}

void SceneryAbstract::render(wxDC& dc, const CoordinateTransformer& transformer) const {
    dc.SetPen(*wxGREEN_PEN);
    dc.SetBrush(wxNullBrush);
    auto point = transformer(coordinate);
    dc.DrawRectangle(point - wxPoint{3, 3}, {6, 6});
}

void SceneryBase::loadBody(std::istream &istream) {
    SceneryAbstract::loadBody(istream);
}

void Scenery::loadBody(std::istream &istream) {
    uint32_t version, b;
    char c, useSoup;
    istream.read(reinterpret_cast<char*>(&version), 4);
    istream.read(reinterpret_cast<char*>(&b), 4);
std::cout << "\t\t\tversion(5): " << version << ' ' << std::hex << b << std::dec;
    if (version > 4) {
        istream.read(&c, 1);
std::cout << ' ' << static_cast<int>(c);
    }
std::cout << std::endl;
    if (version > 3) {
        istream.read(&useSoup, 1);
std::cout << std::dec << "\t\t\tuseSoup: " << static_cast<int>(useSoup) << std::endl;
        if (useSoup) {
            std::string f;
            f.resize(4);
            istream.read(reinterpret_cast<char *>(f.data()), 4);
            int32_t a1, a2;
            int32_t soupSize;
            istream.read(reinterpret_cast<char *>(&a1), 4);
            istream.read(reinterpret_cast<char *>(&a2), 4);
            istream.read(reinterpret_cast<char *>(&soupSize), 4);
            std::cout << "\t\t\t??: " << f << ", " << a1 << ' ' << a2 << ", size: " << soupSize << std::endl;
            std::string soup;
            soup.resize(soupSize);
            istream.read(soup.data(), soupSize);
        }
    }

    SceneryAbstract::loadBody(istream);

//    if (a < 5) {
//        return;
//    }
    uint32_t len;
    istream.read(reinterpret_cast<char*>(&len), 4);
    name.resize(len);
    istream.read(name.data(), len);
    if ('\0' == name.back()) {
        name.erase(name.length() - 1);
    }
std::cout << "\t\t\tname: \"" << name << '\"' << std::endl;
}

void Camera::loadBody(std::istream &istream) {
    Scenery::loadBody(istream);
    int32_t a, b;
    istream.read(reinterpret_cast<char*>(&a), 4);
    coordinate2.load(istream);
    istream.read(reinterpret_cast<char*>(&b), 4);
std::cout << "\t\t\t??: " << a << ", coord: " << coordinate2 << ", type: " << b << std::endl;
}
