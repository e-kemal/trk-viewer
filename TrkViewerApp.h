#ifndef TRK_VIEWER_TRKVIEWERAPP_H
#define TRK_VIEWER_TRKVIEWERAPP_H


#include <wx/app.h>
#include "World.h"

class TrkViewerApp : public wxApp {
    World world;
public:
    bool OnInit() override;
};


#endif //TRK_VIEWER_TRKVIEWERAPP_H
